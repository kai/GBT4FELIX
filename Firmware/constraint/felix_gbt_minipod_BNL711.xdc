###############################################################################
# User Configuration
# Link Width   - x8
# Link Speed   - gen3
# Family       - virtex7
# Part         - xc7vx690t
# Package      - ffg1761
# Speed grade  - -2
# PCIe Block   - X0Y1
###############################################################################
#
###############################################################################
# User Constraints
###############################################################################

## MiniPOD  Reset Pins
#set_property PACKAGE_PIN G14 [get_ports minipod_rst_tx]
## High to disable reset, and enable the MiniPOD
#set_property IOSTANDARD LVCMOS33 [get_ports minipod_rst_tx]
#set_property PACKAGE_PIN K10 [get_ports minipod_rst_rx]
#set_property IOSTANDARD LVCMOS33 [get_ports minipod_rst_rx]

#set_property PACKAGE_PIN H14 [get_ports minipod_rst_tx2]
## High to disable reset, and enable the MiniPOD
#set_property IOSTANDARD LVCMOS33 [get_ports minipod_rst_tx2]
#set_property PACKAGE_PIN K11 [get_ports minipod_rst_rx2]
#set_property IOSTANDARD LVCMOS33 [get_ports minipod_rst_rx2]

#set_property PACKAGE_PIN G14 [get_ports opto_inhibit[0]]
#set_property IOSTANDARD LVCMOS33 [get_ports opto_inhibit[0]]

#set_property PACKAGE_PIN K10 [get_ports opto_inhibit[1]]
#set_property IOSTANDARD LVCMOS33 [get_ports opto_inhibit[1]]

#NET RX_CLK_TP LOC = AM31 | IOSTANDARD = "LVCMOS18";

###############################################################################
# User Time Names / User Time Groups / Time Specs
###############################################################################

####################### GT reference clock constraints #######################

## GT Reference clocks for Q1Q2Q3 and Q7Q8Q9

#set_property PACKAGE_PIN V38 [get_ports Q8_CLK0_GTREFCLK_PAD_N_IN]
#set_property PACKAGE_PIN V37 [get_ports Q8_CLK0_GTREFCLK_PAD_P_IN]
#set_property PACKAGE_PIN AK38 [get_ports Q2_CLK0_GTREFCLK_PAD_N_IN]
#set_property PACKAGE_PIN AK37 [get_ports Q2_CLK0_GTREFCLK_PAD_P_IN]

###############################################################################
# GBT CXP Physical Constraints
###############################################################################

### CXP (physical) channels to GBT (firmware) channels mapping
### Note: the below lines in FELIX_Gbt_wrapper.vhd should be consistent with
### the mapping when using the GT Reference clock
### GTH_RefClk(0) <= CXP1_GTH_RefClk;
### GTH_RefClk(1) <= CXP1_GTH_RefClk;
### GTH_RefClk(2) <= CXP1_GTH_RefClk;
### GTH_RefClk(3) <= CXP2_GTH_RefClk;
### GTH_RefClk(4) <= CXP2_GTH_RefClk;
### GTH_RefClk(5) <= CXP2_GTH_RefClk;

### pblocks are used to constraint the TxGearBox, GTH FSMs, and RxGearBox
#
#bank 126, 127, 128 use clk from bank 127
set_property PACKAGE_PIN AH37 [get_ports Q2_CLK0_GTREFCLK_PAD_P_IN]
set_property PACKAGE_PIN AH38 [get_ports Q2_CLK0_GTREFCLK_PAD_N_IN]
#bank 131, 132, 133 use clk from bank 132
set_property PACKAGE_PIN T37 [get_ports Q8_CLK0_GTREFCLK_PAD_P_IN]
set_property PACKAGE_PIN T38 [get_ports Q8_CLK0_GTREFCLK_PAD_N_IN]


#set_property PACKAGE_PIN AL14 [get_ports TP1_P]
#set_property IOSTANDARD LVDS [get_ports TP1_P]
#set_property IOSTANDARD LVDS [get_ports TP1_N]

#set_property PACKAGE_PIN K22     [get_ports TP2_P]
#set_property IOSTANDARD LVDS [get_ports TP2_P]
#set_property IOSTANDARD LVDS [get_ports TP2_N]


# channel 0-11
set_property PACKAGE_PIN V38 [get_ports {MGTREFCLK_LOCX2_n[0]}]
set_property PACKAGE_PIN V37 [get_ports {MGTREFCLK_LOCX2_p[0]}]
#ch 12-23,28-35
set_property PACKAGE_PIN P7 [get_ports {MGTREFCLK_LOCX2_n[1]}]
set_property PACKAGE_PIN P8 [get_ports {MGTREFCLK_LOCX2_p[1]}]

#ch 24-27
set_property PACKAGE_PIN AF7 [get_ports {MGTREFCLK_LOCX2_n[2]}]
set_property PACKAGE_PIN AF8 [get_ports {MGTREFCLK_LOCX2_p[2]}]
#ch 36-39
set_property PACKAGE_PIN AK38 [get_ports {MGTREFCLK_LOCX2_n[3]}]
set_property PACKAGE_PIN AK37 [get_ports {MGTREFCLK_LOCX2_p[3]}]
#create_clock -name locx2top/GT_RXUSRCLK -period 3.125 [get_pins locx2rc_inst/GTRXOUTCLK_BUFG/O]

set_property PACKAGE_PIN AW44  [get_ports {locrxn_in[39]}]
set_property PACKAGE_PIN BA44  [get_ports {locrxn_in[38]}]
set_property PACKAGE_PIN AR44  [get_ports {locrxn_in[37]}]
set_property PACKAGE_PIN AU44  [get_ports {locrxn_in[36]}]

set_property PACKAGE_PIN R1  [get_ports {locrxn_in[35]}]
set_property PACKAGE_PIN L1  [get_ports {locrxn_in[34]}]
set_property PACKAGE_PIN U1  [get_ports {locrxn_in[33]}]
set_property PACKAGE_PIN N1  [get_ports {locrxn_in[32]}]
set_property PACKAGE_PIN AA1  [get_ports {locrxn_in[31]}]
set_property PACKAGE_PIN V3  [get_ports {locrxn_in[30]}]
set_property PACKAGE_PIN AB3  [get_ports {locrxn_in[29]}]
set_property PACKAGE_PIN W1  [get_ports {locrxn_in[28]}]
set_property PACKAGE_PIN AE1  [get_ports {locrxn_in[27]}]
set_property PACKAGE_PIN AC1  [get_ports {locrxn_in[26]}]
set_property PACKAGE_PIN AF3  [get_ports {locrxn_in[25]}]
set_property PACKAGE_PIN AG1  [get_ports {locrxn_in[24]}]

set_property PACKAGE_PIN E9  [get_ports {locrxn_in[23]}]
set_property PACKAGE_PIN B11  [get_ports {locrxn_in[22]}]
set_property PACKAGE_PIN A5  [get_ports {locrxn_in[21]}]
set_property PACKAGE_PIN C9  [get_ports {locrxn_in[10]}]
set_property PACKAGE_PIN D3  [get_ports {locrxn_in[19]}]
set_property PACKAGE_PIN B3  [get_ports {locrxn_in[18]}]
set_property PACKAGE_PIN E1  [get_ports {locrxn_in[17]}]
set_property PACKAGE_PIN C1  [get_ports {locrxn_in[16]}]
set_property PACKAGE_PIN F3  [get_ports {locrxn_in[15]}]
set_property PACKAGE_PIN G1  [get_ports {locrxn_in[14]}]
set_property PACKAGE_PIN H3  [get_ports {locrxn_in[13]}]
set_property PACKAGE_PIN J1  [get_ports {locrxn_in[12]}]

set_property PACKAGE_PIN AA44  [get_ports {locrxn_in[11]}]
set_property PACKAGE_PIN AB42  [get_ports {locrxn_in[10]}]
set_property PACKAGE_PIN V42  [get_ports {locrxn_in[9]}]
set_property PACKAGE_PIN W44  [get_ports {locrxn_in[8]}]
set_property PACKAGE_PIN U44  [get_ports {locrxn_in[7]}]
set_property PACKAGE_PIN R44  [get_ports {locrxn_in[6]}]
set_property PACKAGE_PIN N44  [get_ports {locrxn_in[5]}]
set_property PACKAGE_PIN L44  [get_ports {locrxn_in[4]}]
set_property PACKAGE_PIN J44  [get_ports {locrxn_in[3]}]
set_property PACKAGE_PIN G44  [get_ports {locrxn_in[2]}]
set_property PACKAGE_PIN E44  [get_ports {locrxn_in[1]}]
set_property PACKAGE_PIN C44  [get_ports {locrxn_in[0]}]

###############################################################################
# Physical Constraints
###############################################################################

## MultiCycle paths for the GBT decoding

#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" } ] 4
#set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" } ] 3

#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" } ] 4
#set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" } ] 3

#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" } ] 4
#set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" } ] 3

#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" } ] 4
#set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" } ] 3


set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" } ] 2

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" } ] 2

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" } ] 2

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" } ] 2


set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[0].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 2
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[0].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 1
set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[1].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 2
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[1].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 1
set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[2].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 2
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[2].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 1

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[0].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[0].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 2
set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[1].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[1].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 2


set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" } ] 2
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" } ] 1

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" } ] 2
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" } ] 1

## Already in timing constraint: need much time
#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" } ] 1
#set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" } ] 0
#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" } ] 1
#set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" } ] 0
#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" } ] 1
#set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" } ] 0



###############################################################################
# Timing Constraints
###############################################################################


create_clock -name g1.u2/GT_TX_WORD_CLK[0] -period 4.167 [get_pins g1.u2/clk_generate[0].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[1] -period 4.167 [get_pins g1.u2/clk_generate[1].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[2] -period 4.167 [get_pins g1.u2/clk_generate[2].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[3] -period 4.167 [get_pins g1.u2/clk_generate[3].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[4] -period 4.167 [get_pins g1.u2/clk_generate[4].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[5] -period 4.167 [get_pins g1.u2/clk_generate[5].GTTXOUTCLK_BUFG/O]

create_clock -name g1.u2/GT_TX_WORD_CLK[6] -period 4.167 [get_pins g1.u2/clk_generate[6].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[7] -period 4.167 [get_pins g1.u2/clk_generate[7].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[8] -period 4.167 [get_pins g1.u2/clk_generate[8].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[9] -period 4.167 [get_pins g1.u2/clk_generate[9].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[10] -period 4.167 [get_pins g1.u2/clk_generate[10].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[11] -period 4.167 [get_pins g1.u2/clk_generate[11].GTTXOUTCLK_BUFG/O]

create_clock -name g1.u2/GT_TX_WORD_CLK[12] -period 4.167 [get_pins g1.u2/clk_generate[12].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[13] -period 4.167 [get_pins g1.u2/clk_generate[13].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[14] -period 4.167 [get_pins g1.u2/clk_generate[14].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[15] -period 4.167 [get_pins g1.u2/clk_generate[15].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[16] -period 4.167 [get_pins g1.u2/clk_generate[16].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[17] -period 4.167 [get_pins g1.u2/clk_generate[17].GTTXOUTCLK_BUFG/O]

create_clock -name g1.u2/GT_TX_WORD_CLK[18] -period 4.167 [get_pins g1.u2/clk_generate[18].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[19] -period 4.167 [get_pins g1.u2/clk_generate[19].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[20] -period 4.167 [get_pins g1.u2/clk_generate[20].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[21] -period 4.167 [get_pins g1.u2/clk_generate[21].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[22] -period 4.167 [get_pins g1.u2/clk_generate[22].GTTXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_TX_WORD_CLK[23] -period 4.167 [get_pins g1.u2/clk_generate[23].GTTXOUTCLK_BUFG/O]


create_clock -name g1.u2/GT_RX_WORD_CLK[0] -period 4.167 [get_pins g1.u2/clk_generate[0].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[1] -period 4.167 [get_pins g1.u2/clk_generate[1].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[2] -period 4.167 [get_pins g1.u2/clk_generate[2].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[3] -period 4.167 [get_pins g1.u2/clk_generate[3].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[4] -period 4.167 [get_pins g1.u2/clk_generate[4].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[5] -period 4.167 [get_pins g1.u2/clk_generate[5].GTRXOUTCLK_BUFG/O]

create_clock -name g1.u2/GT_RX_WORD_CLK[6] -period 4.167 [get_pins g1.u2/clk_generate[6].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[7] -period 4.167 [get_pins g1.u2/clk_generate[7].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[8] -period 4.167 [get_pins g1.u2/clk_generate[8].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[9] -period 4.167 [get_pins g1.u2/clk_generate[9].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[10] -period 4.167 [get_pins g1.u2/clk_generate[10].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[11] -period 4.167 [get_pins g1.u2/clk_generate[11].GTRXOUTCLK_BUFG/O]

create_clock -name g1.u2/GT_RX_WORD_CLK[12] -period 4.167 [get_pins g1.u2/clk_generate[12].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[13] -period 4.167 [get_pins g1.u2/clk_generate[13].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[14] -period 4.167 [get_pins g1.u2/clk_generate[14].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[15] -period 4.167 [get_pins g1.u2/clk_generate[15].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[16] -period 4.167 [get_pins g1.u2/clk_generate[16].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[17] -period 4.167 [get_pins g1.u2/clk_generate[17].GTRXOUTCLK_BUFG/O]

create_clock -name g1.u2/GT_RX_WORD_CLK[18] -period 4.167 [get_pins g1.u2/clk_generate[18].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[19] -period 4.167 [get_pins g1.u2/clk_generate[19].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[20] -period 4.167 [get_pins g1.u2/clk_generate[20].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[21] -period 4.167 [get_pins g1.u2/clk_generate[21].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[22] -period 4.167 [get_pins g1.u2/clk_generate[22].GTRXOUTCLK_BUFG/O]
create_clock -name g1.u2/GT_RX_WORD_CLK[23] -period 4.167 [get_pins g1.u2/clk_generate[23].GTRXOUTCLK_BUFG/O]


#create_clock -name g1.u2/GT_RX_WORD_CLK[0] -period 4.167 [get_pins g1.u2/usrclk_inst[0].gthusrclk_gen/clkgen_sys.rxoutclk_bufg1_i/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[4] -period 4.167 [get_pins g1.u2/usrclk_inst[1].gthusrclk_gen/clkgen_sys.rxoutclk_bufg1_i/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[8] -period 4.167 [get_pins g1.u2/usrclk_inst[2].gthusrclk_gen/clkgen_sys.rxoutclk_bufg1_i/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[12] -period 4.167 [get_pins g1.u2/usrclk_inst[3].gthusrclk_gen/clkgen_sys.rxoutclk_bufg1_i/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[16] -period 4.167 [get_pins g1.u2/usrclk_inst[4].gthusrclk_gen/clkgen_sys.rxoutclk_bufg1_i/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[20] -period 4.167 [get_pins g1.u2/usrclk_inst[5].gthusrclk_gen/clkgen_sys.rxoutclk_bufg1_i/O]



#set_false_path -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[0]]
#set_false_path -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[0]]
#set_multicycle_path 3 -setup -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[0]]
#set_multicycle_path 2 -hold -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[0]]
#set_multicycle_path 3 -setup -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[0]]
#set_multicycle_path 2 -hold -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[0]]


#set_false_path -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[4]]
#set_false_path -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[4]]
#set_multicycle_path 3 -setup -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[4]]
#set_multicycle_path 2 -hold -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[4]]
#set_multicycle_path 3 -setup -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[4]]
#set_multicycle_path 2 -hold -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[4]]
#set_false_path -from [get_clocks ts_clk_adn_160] -to [get_clocks g1.u2/GT_TX_WORD_CLK[4]]

#set_false_path -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[20]]
#set_false_path -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[20]]
#set_multicycle_path 3 -setup -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[20]]
#set_multicycle_path 2 -hold -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[20]]
#set_multicycle_path 3 -setup -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[20]]
#set_multicycle_path 2 -hold -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[20]]
#set_false_path -from [get_clocks ts_clk_adn_160] -to [get_clocks g1.u2/GT_TX_WORD_CLK[20]]

#set_false_path -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[16]]
#set_false_path -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[16]]
#set_multicycle_path 3 -setup -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[16]]
#set_multicycle_path 2 -hold -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[16]]
#set_multicycle_path 3 -setup -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[16]]
#set_multicycle_path 2 -hold -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[16]]
#set_false_path -from [get_clocks ts_clk_adn_160] -to [get_clocks g1.u2/GT_TX_WORD_CLK[16]]

#set_false_path -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[12]]
#set_false_path -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[12]]
#set_multicycle_path 3 -setup -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[12]]
#set_multicycle_path 2 -hold -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[12]]
#set_multicycle_path 3 -setup -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[12]]
#set_multicycle_path 2 -hold -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[12]]
#set_false_path -from [get_clocks ts_clk_adn_160] -to [get_clocks g1.u2/GT_TX_WORD_CLK[12]]

#set_false_path -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[8]]
#set_false_path -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[8]]
#set_multicycle_path 3 -setup -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[8]]
#set_multicycle_path 2 -hold -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks g1.u2/GT_TX_WORD_CLK[8]]
#set_multicycle_path 3 -setup -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[8]]
#set_multicycle_path 2 -hold -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks g1.u2/GT_TX_WORD_CLK[8]]
#set_false_path -from [get_clocks ts_clk_adn_160] -to [get_clocks g1.u2/GT_TX_WORD_CLK[8]]


#set_false_path -from [get_clocks g1.u2/GT_TX_WORD_CLK[0]] -to [get_clocks clk40_clk_wiz_40_0]
#set_false_path -from [get_clocks g1.u2/GT_TX_WORD_CLK[4]] -to [get_clocks clk40_clk_wiz_40_0]
#set_false_path -from [get_clocks g1.u2/GT_TX_WORD_CLK[8]] -to [get_clocks clk40_clk_wiz_40_0]
#set_false_path -from [get_clocks g1.u2/GT_TX_WORD_CLK[12]] -to [get_clocks clk40_clk_wiz_40_0]
#set_false_path -from [get_clocks g1.u2/GT_TX_WORD_CLK[16]] -to [get_clocks clk40_clk_wiz_40_0]
#set_false_path -from [get_clocks g1.u2/GT_TX_WORD_CLK[20]] -to [get_clocks clk40_clk_wiz_40_0]
#
#set_false_path -from [get_clocks {g1.u2/GT_TX_WORD_CLK[0]}] -to [get_clocks clk_out40_clk_wiz_40]
#set_false_path -from [get_clocks {g1.u2/GT_TX_WORD_CLK[4]}] -to [get_clocks clk_out40_clk_wiz_40]
#set_false_path -from [get_clocks {g1.u2/GT_TX_WORD_CLK[8]}] -to [get_clocks clk_out40_clk_wiz_40]
#set_false_path -from [get_clocks {g1.u2/GT_TX_WORD_CLK[12]}] -to [get_clocks clk_out40_clk_wiz_40]
#set_false_path -from [get_clocks {g1.u2/GT_TX_WORD_CLK[16]}] -to [get_clocks clk_out40_clk_wiz_40]
#set_false_path -from [get_clocks {g1.u2/GT_TX_WORD_CLK[20]}] -to [get_clocks clk_out40_clk_wiz_40]

###############################################################################
# Others
###############################################################################

# force Vivado to ignore usage of GTGREFCLK
#set_property SEVERITY {Warning} [get_drc_checks REQP-44]
#set_property SEVERITY {Warning} [get_drc_checks REQP-46]
# force vivado to ignore unplaced pins
#set_property SEVERITY {Warning} [get_drc_checks IOSTDTYPE-1]

#set_false_path -to [get_cells -hierarchical -filter {NAME =~ *bit_synchronizer*inst/i_in_meta_reg}]
#set_false_path -to [get_cells -hierarchical -filter {NAME =~ *reset_synchronizer*inst/rst_in_*_reg}]

###############################################################################
# End
###############################################################################
