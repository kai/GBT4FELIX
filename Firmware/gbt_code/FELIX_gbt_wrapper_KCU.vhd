--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: FELIX BNL-711 GBT Wrapper
-- Module Name: gbt_top - Behavioral
-- Project Name:
-- Target Devices: KCU
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE FOR FELIX GBT & GTH
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

LIBRARY IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;
use work.FELIX_gbt_package.all;
use work.pcie_package.all;

entity FELIX_gbt_wrapper_KCU is
  Generic (
    STABLE_CLOCK_PERIOD         : integer   := 24;  --period of the drp_clock
    GBT_NUM                     : integer := 24;
    GTHREFCLK_SEL               : std_logic; --GREFCLK                                      : std_logic := '1';
                                             --MGTREFCLK                                    : std_logic := '0';
    PLL_SEL                     : std_logic       -- CPLL : '0'
                                                  -- QPLL : '1'
    --QUAD_NUM                  : integer := 6
    );
  Port (
-------------------
---- For debug
-------------------
    -- For Debugging
    RX_FLAG_O                   : out std_logic_vector(GBT_NUM-1 downto 0);
    TX_FLAG_O                   : out std_logic_vector(GBT_NUM-1 downto 0);
    REFCLK_CXP1                 : out std_logic;
    REFCLK_CXP2                 : out std_logic;

    rst_hw                      : in std_logic;

    register_map_control        : in register_map_control_type;
    register_map_gbt_monitor    : out register_map_gbt_monitor_type;
    
    ICECBUSY : in std_logic_vector(15 downto 0);
           SYNC_STATUS_MON: in std_logic_vector(79 downto 0);
             cdr_lock ,crc_mon : in std_logic_vector(39 downto 0);
               RXRST_DONE  : in std_logic_vector(9 downto 0);
               TXRST_DONE  : in std_logic_vector(9 downto 0);
               cdr_stable  : in std_logic_vector(9 downto 0);
               err_1_cnt, err_2_cnt, err_3_cnt,err_4_cnt: in std_logic_vector(19 downto 0);
          ICEC_INT_RX_DATA: IN txrx64b_24ch_type;
          ddra_cal_done,ddrb_cal_done : in std_logic;
    -- GTH REFCLK, DRPCLK, GREFCLK
    DRP_CLK_IN                  : in std_logic;
    Q2_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
    Q2_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
    Q8_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
    Q8_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
    Q4_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
    Q4_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
    Q5_CLK0_GTREFCLK_PAD_N_IN   : in std_logic;
    Q5_CLK0_GTREFCLK_PAD_P_IN   : in std_logic;
    GREFCLK_IN                  : in std_logic;

    clk40_in                    : in std_logic;
    clk240_in                   : in std_logic;
    -- for CentralRouter
    TX_120b_in                  : in  txrx120b_type(0 to GBT_NUM-1);
    RX_120b_out                 : out txrx120b_type(0 to GBT_NUM-1);
    FRAME_LOCKED_O              : out std_logic_vector(GBT_NUM-1 downto 0);
    -- TX_ISDATA_I              : in std_logic_vector(GBT_NUM-1 downto 0);
    -- RX_ISDATA_O              : out std_logic_vector(GBT_NUM-1 downto 0);
    -- RX_FRAME_CLK_O           : out std_logic_vector(GBT_NUM-1 downto 0);
    TX_FRAME_CLK_I              : in std_logic_vector(GBT_NUM-1 downto 0);

    -- FIFO_RD_CLK              : in std_logic_vector(GBT_NUM-1 downto 0);
    -- FIFO_RD_EN               : in std_logic_vector(GBT_NUM-1 downto 0);
    -- FIFO_FULL                : out std_logic_vector(GBT_NUM-1 downto 0);
    -- FIFO_EMPTY               : out std_logic_vector(GBT_NUM-1 downto 0);


    -- GTH Data pins
    TX_P                        : out std_logic_vector(GBT_NUM-1 downto 0);
    TX_N                        : out std_logic_vector(GBT_NUM-1 downto 0);
    RX_P                        : in  std_logic_vector(GBT_NUM-1 downto 0);
    RX_N                        : in  std_logic_vector(GBT_NUM-1 downto 0)

);
end FELIX_gbt_wrapper_KCU;

architecture Behavioral of FELIX_gbt_wrapper_KCU is



  component fifo_GBT2CR IS
    PORT (
      wr_clk    : IN STD_LOGIC;
      wr_rst    : IN STD_LOGIC;
      rd_clk    : IN STD_LOGIC;
      rd_rst    : IN STD_LOGIC;
      din       : IN STD_LOGIC_VECTOR(119 DOWNTO 0);
      wr_en     : IN STD_LOGIC;
      rd_en     : IN STD_LOGIC;
      dout      : OUT STD_LOGIC_VECTOR(119 DOWNTO 0);
      full      : OUT STD_LOGIC;
      empty     : OUT STD_LOGIC;
      prog_empty : OUT STD_LOGIC
      );
  END component;

 -- constant QUAD_NUM : integer := GBT_NUM / 4;

  signal rxslide_manual : STD_LOGIC_VECTOR(47 downto 0);
  signal RxSlide_c      : STD_LOGIC_VECTOR(47 downto 0);
  signal RxSlide_i      : std_logic_vector(47 downto 0);
  signal rxslide_sel    : std_logic_vector(47 downto 0);
  signal txusrrdy       : std_logic_vector(47 downto 0);
  signal rxusrrdy       : std_logic_vector(47 downto 0);
  signal gttx_reset     : std_logic_vector(47 downto 0);

  signal gtrx_reset     : std_logic_vector(47 downto 0);
  signal soft_reset     : std_logic_vector(47 downto 0);
  signal cpll_reset     : std_logic_vector(47 downto 0);
  signal qpll_reset     : std_logic_vector(11 downto 0);
  signal txresetdone    : std_logic_vector(47 downto 0);

  signal clk_sampled    : std_logic_vector(47 downto 0);

  signal rxresetdone    : std_logic_vector(47 downto 0);
  signal txfsmresetdone : std_logic_vector(47 downto 0);
  signal rxfsmresetdone : std_logic_vector(47 downto 0);
  signal cpllfbclklost  : std_logic_vector(47 downto 0);
  signal cplllock       : std_logic_vector(47 downto 0);
  signal rxcdrlock      : std_logic_vector(47 downto 0);
  signal qplllock       : std_logic_vector(11 downto 0);
  signal cdr_cnt        : std_logic_vector(8 downto 0);

  signal tx_is_data     : std_logic_vector(47 downto 0);
  signal TX_RESET       : std_logic_vector(47 downto 0);
  signal TX_RESET_i     : std_logic_vector(47 downto 0);

  signal RX_RESET       : std_logic_vector(47 downto 0);
  signal RX_RESET_i     : std_logic_vector(47 downto 0);
  signal GT_TXUSRCLK    : std_logic_vector(47 downto 0);
  signal GT_RXUSRCLK    : std_logic_vector(47 downto 0);
  signal RX_FLAG_Oi     : std_logic_vector(47 downto 0);
  signal gbt_data_format: std_logic_vector(47 downto 0);

  SIGNAL RX_ALIGN_SW    : STD_logic;
  signal RX_ALIGN_TB_SW : STD_logic;

  signal rx_pll_locked  : std_logic_vector(47 downto 0);
  signal outsel_i       : std_logic_vector(47 downto 0);
  signal outsel_ii      : std_logic_vector(47 downto 0);
  signal outsel_o       : std_logic_vector(47 downto 0);
  signal RX_120b_out_i  : txrx120b_type(0 to (GBT_NUM-1));
  signal RX_120b_out_ii : txrx120b_type(0 to (GBT_NUM-1));

  signal rx_is_header   : std_logic_vector(47 downto 0);
  signal alignment_done : std_logic_vector(47 downto 0);
  signal rx_is_data     : std_logic_vector(47 downto 0);
  signal RX_HEADER_FOUND: std_logic_vector(47 downto 0);

  signal RxSlide        : std_logic_vector(47 downto 0);

  signal GT_TX_WORD_CLK : std_logic_vector(47 downto 0);
  signal TX_TC_METHOD   : std_logic_vector(47 downto 0);
  signal TC_EDGE        : std_logic_vector(47 downto 0);

  type data20barray     is array (0 to 47) of std_logic_vector(19 downto 0);
  signal TX_DATA_20b    : data20barray := (others => ("00000000000000000000"));
  signal RX_DATA_20b    : data20barray := (others => ("00000000000000000000"));

  signal GT_RX_WORD_CLK         : std_logic_vector(47 downto 0);
  signal alignment_chk_rst_c    : std_logic_vector(47 downto 0);
  signal alignment_chk_rst_c1   : std_logic_vector(47 downto 0);
  signal alignment_chk_rst      : std_logic_vector(47 downto 0);
  signal alignment_chk_rst_f    : std_logic_vector(47 downto 0);

  signal rstframeclk            : std_logic;
  signal alignment_chk_rst_i    : std_logic;
  signal rstframeclk1           : std_logic;

  signal DESMUX_USE_SW          : std_logic;

  signal rstframeclk_3r         : std_logic;
  signal rstframeclk_r          : std_logic;
  signal rstframeclk_2r         : std_logic;
  signal rstframeclk1_3r        : std_logic;
  signal rstframeclk1_r         : std_logic;
  signal rstframeclk1_2r        : std_logic;
  signal cxp1_tx_pll_rst        : std_logic;
  signal cxp2_tx_pll_rst        : std_logic;
  signal SOFT_TXRST_GT          : std_logic_vector(47 downto 0);
  signal TopBot                 : std_logic_vector(47 downto 0);
  signal TopBot_C               : std_logic_vector(47 downto 0);
  signal TopBot_i               : std_logic_vector(47 downto 0);
  signal SOFT_RXRST_GT          : std_logic_vector(47 downto 0);
  signal SOFT_TXRST_ALL         : std_logic_vector(11 downto 0);
  signal SOFT_RXRST_ALL         : std_logic_vector(11 downto 0);
  signal TX_OPT                 : std_logic_vector(95 downto 0);
  signal RX_OPT                 : std_logic_vector(95 downto 0);
  SIGNAL DATA_TXFORMAT          : std_logic_vector(47 downto 0);
  signal DATA_TXFORMAT_i        : std_logic_vector(47 downto 0);
  SIGNAL DATA_RXFORMAT          : std_logic_vector(47 downto 0);
  signal DATA_RXFORMAT_i        : std_logic_vector(47 downto 0);

  SIGNAL OddEven                : std_logic_vector(47 downto 0);
  signal OddEven_i              : std_logic_vector(47 downto 0);
  signal OddEven_c              : std_logic_vector(47 downto 0);
  signal ext_trig_realign       : std_logic_vector(47 downto 0);



  signal General_ctrl           : std_logic_vector(63 downto 0);


  signal GBT_RXSLIDE            : std_logic_vector(47 downto 0);
  signal GBT_TXUSRRDY           : std_logic_vector(47 downto 0);
  signal GBT_RXUSRRDY           : std_logic_vector(47 downto 0);
  signal GBT_GTTX_RESET         : std_logic_vector(47 downto 0);
  signal GBT_GTRX_RESET         : std_logic_vector(47 downto 0);
  signal GBT_PLL_RESET          : std_logic_vector(47 downto 0);
  signal GBT_SOFT_TX_RESET      : std_logic_vector(47 downto 0);
  signal GBT_SOFT_RX_RESET      : std_logic_vector(47 downto 0);
  signal GBT_ODDEVEN            : std_logic_vector(47 downto 0);
  signal GBT_TOPBOT             : std_logic_vector(47 downto 0);
  signal GBT_TX_TC_DLY_VALUE1   : std_logic_vector(47 downto 0);
  signal GBT_TX_TC_DLY_VALUE2   : std_logic_vector(47 downto 0);
  signal GBT_TX_OPT             : std_logic_vector(47 downto 0);
  signal GBT_RX_OPT             : std_logic_vector(47 downto 0);
  signal GBT_DATA_TXFORMAT      : std_logic_vector(47 downto 0);
  signal GBT_DATA_RXFORMAT      : std_logic_vector(47 downto 0);
  signal GBT_TX_RESET           : std_logic_vector(47 downto 0);
  signal GBT_RX_RESET           : std_logic_vector(47 downto 0);
  signal GBT_TX_TC_METHOD       : std_logic_vector(47 downto 0);
  signal GBT_TC_EDGE            : std_logic_vector(47 downto 0);
  signal GBT_OUTMUX_SEL         : std_logic_vector(47 downto 0);

  SIGNAL GBT_TXRESET_DONE       : std_logic_vector(47 downto 0);
  SIGNAL GBT_RXRESET_DONE       : std_logic_vector(47 downto 0);
  signal TXPMARESETDONE         : std_logic_vector(47 downto 0);
  signal RXPMARESETDONE         : std_logic_vector(47 downto 0);
  signal alignment_done_f       : std_logic_vector(47 downto 0);
  signal soft_reset_f           : std_logic_vector(47 downto 0);
  signal fifo_empty             : std_logic_vector(47 downto 0);
  signal userclk_rx_reset_in    : std_logic_vector(47 downto 0);
  signal userclk_tx_reset_in    : std_logic_vector(47 downto 0);
  signal TXPMARESETDONE_out     : std_logic_vector(47 downto 0);
  signal RXPMARESETDONE_out     : std_logic_vector(47 downto 0);

  SIGNAL GBT_TXFSMRESET_DONE    : std_logic_vector(47 downto 0);
  SIGNAL GBT_RXFSMRESET_DONE    : std_logic_vector(47 downto 0);
  SIGNAL GBT_CPLL_FBCLK_LOST    : std_logic_vector(47 downto 0);
  SIGNAL GBT_PLL_LOCK           : std_logic_vector(47 downto 0);
  SIGNAL GBT_RXCDR_LOCK         : std_logic_vector(47 downto 0);
  SIGNAL GBT_CLK_SAMPLED        : std_logic_vector(47 downto 0);
  SIGNAL GBT_RX_IS_HEADER       : std_logic_vector(47 downto 0);
  SIGNAL GBT_RX_IS_DATA         : std_logic_vector(47 downto 0);
  SIGNAL GBT_RX_HEADER_FOUND    : std_logic_vector(47 downto 0);
  SIGNAL GBT_ALIGNMENT_DONE     : std_logic_vector(47 downto 0);
  SIGNAL GBT_OUT_MUX_STATUS     : std_logic_vector(47 downto 0);
  SIGNAL GBT_ERROR              : std_logic_vector(47 downto 0);
  SIGNAL GBT_GBT_TOPBOT_C       : std_logic_vector(47 downto 0);
  signal rxcdrlock_a            : std_logic_vector(47 downto 0);

  SIGNAL LOGIC_RST              : std_logic_vector(63 downto 0);
  SIGNAL Mode_ctrl              : std_logic_vector(47 downto 0);
  SIGNAL TX_TC_DLY_VALUE        : std_logic_vector(95 downto 0);
  signal data_sel               : std_logic_vector(95 downto 0);

  signal GTH_RefClk             : std_logic_vector(47 downto 0);
  signal gbt_sel                : std_logic_vector(47 downto 0);
  signal lock_lg                : std_logic_vector(47 downto 0);
  signal pulse_cnt              : std_logic_vector(29 downto 0);
  signal pulse_lg               : std_logic;

  signal CXP1_GTH_RefClk        : std_logic;
  signal CXP2_GTH_RefClk        : std_logic;
  signal CXP4_GTH_RefClk        : std_logic;
  signal CXP3_GTH_RefClk        : std_logic;
  signal des_rxusrclk_cxp1      : std_logic;
  signal des_rxusrclk_cxp2      : std_logic;

  signal alignment_done_chk_cnt : std_logic_vector(12 downto 0);
  signal alignment_done_a       : std_logic_vector(47 downto 0);
  signal clksampled             : std_logic_vector(47 downto 0);
  signal des_rxusrclk           : std_logic_vector(47 downto 0);
  signal error_orig             : std_logic_vector(47 downto 0);
  signal error_f                : std_logic_vector(47 downto 0);
  signal FSM_RST                : std_logic_vector(47 downto 0);
  signal auto_gth_rxrst         : std_logic_vector(47 downto 0);
  signal auto_gbt_rxrst         : std_logic_vector(47 downto 0);
  signal gbt_rx_reset_i         : std_logic_vector(47 downto 0);
  signal gtrx_reset_i           : std_logic_vector(23 downto 0);

  signal TX_LINERATE            : std_logic_vector(47 downto 0);
  signal RX_LINERATE            : std_logic_vector(47 downto 0);
  signal GT_RXOUTCLK            : std_logic_vector(47 downto 0);
  signal GT_TXOUTCLK            : std_logic_vector(47 downto 0);

  signal BITSLIP_MANUAL_r       : std_logic_vector(47 downto 0);
  signal BITSLIP_MANUAL_2r      : std_logic_vector(47 downto 0);
  signal BITSLIP_MANUAL_3r      : std_logic_vector(47 downto 0);
  type txrx80b_12ch_type        is array (11 downto 0) of std_logic_vector(79 downto 0);
  signal RX_DATA_80b            : txrx80b_12ch_type;

  signal gttx_reset_merge       : std_logic_vector(11 downto 0);
  signal gtrx_reset_merge       : std_logic_vector(11 downto 0);
  signal rxcdrlock_quad         : std_logic_vector(11 downto 0);
  signal rxresetdone_quad       : std_logic_vector(11 downto 0);
  signal txresetdone_quad       : std_logic_vector(11 downto 0);
  signal rxcdrlock_out          : std_logic_vector(47 downto 0);
  signal RxResetDone_f          : std_logic_vector(47 downto 0);
  signal RX_N_i                 : std_logic_vector(47 downto 0):=x"000000000000";
  signal RX_P_i                 : std_logic_vector(47 downto 0):=x"000000000000";
  signal TX_N_i                 : std_logic_vector(47 downto 0):=x"000000000000";
  signal TX_P_i                 : std_logic_vector(47 downto 0):=x"000000000000";  

begin


register_map_gbt_monitor.ICEC_BUSY(15 downto 0) <= ICECBUSY;
register_map_gbt_monitor.IC_RXDATA00 <= ICEC_INT_RX_DATA(0);
register_map_gbt_monitor.IC_RXDATA01 <= ICEC_INT_RX_DATA(1);
register_map_gbt_monitor.IC_RXDATA02 <= ICEC_INT_RX_DATA(2);
register_map_gbt_monitor.IC_RXDATA03 <= ICEC_INT_RX_DATA(3);

register_map_gbt_monitor.EC_RXDATA00 <= ICEC_INT_RX_DATA(4);
register_map_gbt_monitor.EC_RXDATA01 <= ICEC_INT_RX_DATA(5);
register_map_gbt_monitor.EC_RXDATA02 <= ICEC_INT_RX_DATA(6);
register_map_gbt_monitor.EC_RXDATA03 <= ICEC_INT_RX_DATA(7);

register_map_gbt_monitor.EC_RXDATA10 <= ICEC_INT_RX_DATA(8);
register_map_gbt_monitor.EC_RXDATA11 <= ICEC_INT_RX_DATA(9);
register_map_gbt_monitor.EC_RXDATA12 <= ICEC_INT_RX_DATA(10);
register_map_gbt_monitor.EC_RXDATA13 <= ICEC_INT_RX_DATA(11);

register_map_gbt_monitor.EC_RXDATA20 <= ICEC_INT_RX_DATA(12);
register_map_gbt_monitor.EC_RXDATA21 <= ICEC_INT_RX_DATA(13);
register_map_gbt_monitor.EC_RXDATA22 <= ICEC_INT_RX_DATA(14);
register_map_gbt_monitor.EC_RXDATA23 <= ICEC_INT_RX_DATA(15);

register_map_gbt_monitor.EC_RXDATA30 <= ICEC_INT_RX_DATA(16);
register_map_gbt_monitor.EC_RXDATA31 <= ICEC_INT_RX_DATA(17);
register_map_gbt_monitor.EC_RXDATA32 <= ICEC_INT_RX_DATA(18);
register_map_gbt_monitor.EC_RXDATA33 <= ICEC_INT_RX_DATA(19);

register_map_gbt_monitor.EC_RXDATA40 <= ICEC_INT_RX_DATA(20);
register_map_gbt_monitor.EC_RXDATA41 <= ICEC_INT_RX_DATA(21);
register_map_gbt_monitor.EC_RXDATA42 <= ICEC_INT_RX_DATA(22);
register_map_gbt_monitor.EC_RXDATA43 <= ICEC_INT_RX_DATA(23);

register_map_gbt_monitor.LOCx2_CDR_LOCK <= cdr_lock;
register_map_gbt_monitor.LOCx2_LINK_STATUS <= "000000"& ddrb_cal_done & ddra_cal_done & "00" & TXRST_DONE & RXRST_DONE & CDR_STABLE;
register_map_gbt_monitor.LOCx2_ERR_CNT_01 <= err_2_cnt & err_1_cnt;
register_map_gbt_monitor.LOCx2_ERR_CNT_23 <= err_4_cnt & err_3_cnt;
register_map_gbt_monitor.LOCx2_SYNC_STATUS_0 <= SYNC_STATUS_MON(39 DOWNTO 0);
register_map_gbt_monitor.LOCx2_SYNC_STATUS_1 <= SYNC_STATUS_MON(79 DOWNTO 40);
register_map_gbt_monitor.LOCx2_CRC_STATUS <= crc_mon;



  FRAME_LOCKED_O <= RX_HEADER_FOUND(GBT_NUM-1 downto 0);

  -- GTHREFCLK_1 : if GTHREFCLK_SEL = '0' generate
  -- IBUFDS_GTE2
  REFCLK_CXP1 <= CXP1_GTH_RefClk;
  REFCLK_CXP2 <= CXP2_GTH_RefClk;

  ibufds_instq2_clk0 : IBUFDS_GTE3
    port map
    (
      O               => 	CXP1_GTH_RefClk,
      ODIV2           =>    open,
      CEB             => 	'0',
      I               => 	Q2_CLK0_GTREFCLK_PAD_P_IN,
      IB              => 	Q2_CLK0_GTREFCLK_PAD_N_IN
      );

  GTH_RefClk(0)         <= CXP1_GTH_RefClk;
  GTH_RefClk(1)         <= CXP1_GTH_RefClk;
  GTH_RefClk(2)         <= CXP1_GTH_RefClk;
  GTH_RefClk(3)         <= CXP1_GTH_RefClk;
  GTH_RefClk(4)         <= CXP1_GTH_RefClk;
  GTH_RefClk(5)         <= CXP1_GTH_RefClk;
  GTH_RefClk(6)         <= CXP1_GTH_RefClk;
  GTH_RefClk(7)         <= CXP1_GTH_RefClk;

  -- For 16 channels (and below) put 8 channels in SRL0, channel 8..15 in SRL1.
  g_refclk0: if GBT_NUM <= 16 generate
    GTH_RefClk(8)       <= CXP2_GTH_RefClk;
    GTH_RefClk(9)       <= CXP2_GTH_RefClk;
    GTH_RefClk(10)      <= CXP2_GTH_RefClk;
    GTH_RefClk(11)      <= CXP2_GTH_RefClk;
  end generate;

  -- For 24 channels put 12 channels in SRL0, channel 12..23 in SRL1.
  g_refclk1: if GBT_NUM > 16 generate
    GTH_RefClk(8)       <= CXP1_GTH_RefClk;
    GTH_RefClk(9)       <= CXP1_GTH_RefClk;
    GTH_RefClk(10)      <= CXP1_GTH_RefClk;
    GTH_RefClk(11)      <= CXP1_GTH_RefClk;
  end generate;

  --IBUFDS_GTE2
  ibufds_instq8_clk0 : IBUFDS_GTE3
    port map
    (
      O               => 	CXP2_GTH_RefClk,
      ODIV2           =>    open,
      CEB             => 	'0',
      I               => 	Q8_CLK0_GTREFCLK_PAD_P_IN,
      IB              => 	Q8_CLK0_GTREFCLK_PAD_N_IN
      );
  GTH_RefClk(12)        <= CXP2_GTH_RefClk;
  GTH_RefClk(13)        <= CXP2_GTH_RefClk;
  GTH_RefClk(14)        <= CXP2_GTH_RefClk;
  GTH_RefClk(15)        <= CXP2_GTH_RefClk;
  GTH_RefClk(16)        <= CXP2_GTH_RefClk;
  GTH_RefClk(17)        <= CXP2_GTH_RefClk;
  GTH_RefClk(18)        <= CXP2_GTH_RefClk;
  GTH_RefClk(19)        <= CXP2_GTH_RefClk;
  GTH_RefClk(20)        <= CXP2_GTH_RefClk;
  GTH_RefClk(21)        <= CXP2_GTH_RefClk;
  GTH_RefClk(22)        <= CXP2_GTH_RefClk;
  GTH_RefClk(23)        <= CXP2_GTH_RefClk;

  --IBUFDS_GTE2
  ibufds_instq4_clk0 : IBUFDS_GTE3
    port map
    (
      O               => 	CXP3_GTH_RefClk,
      ODIV2           =>    open,
      CEB             => 	'0',
      I               => 	Q4_CLK0_GTREFCLK_PAD_P_IN,
      IB              => 	Q4_CLK0_GTREFCLK_PAD_N_IN
      );
  GTH_RefClk(24)        <= CXP3_GTH_RefClk;
  GTH_RefClk(25)        <= CXP3_GTH_RefClk;
  GTH_RefClk(26)        <= CXP3_GTH_RefClk;
  GTH_RefClk(27)        <= CXP3_GTH_RefClk;
  GTH_RefClk(28)        <= CXP3_GTH_RefClk;
  GTH_RefClk(29)        <= CXP3_GTH_RefClk;
  GTH_RefClk(30)        <= CXP3_GTH_RefClk;
  GTH_RefClk(31)        <= CXP3_GTH_RefClk;
  GTH_RefClk(32)        <= CXP3_GTH_RefClk;
  GTH_RefClk(33)        <= CXP3_GTH_RefClk;
  GTH_RefClk(34)        <= CXP3_GTH_RefClk;
  GTH_RefClk(35)        <= CXP3_GTH_RefClk;
  GTH_RefClk(40)        <= CXP3_GTH_RefClk;
  GTH_RefClk(41)        <= CXP3_GTH_RefClk;
  GTH_RefClk(42)        <= CXP3_GTH_RefClk;
  GTH_RefClk(43)        <= CXP3_GTH_RefClk;
  GTH_RefClk(44)        <= CXP3_GTH_RefClk;
  GTH_RefClk(45)        <= CXP3_GTH_RefClk;
  GTH_RefClk(46)        <= CXP3_GTH_RefClk;
  GTH_RefClk(47)        <= CXP3_GTH_RefClk;

  --IBUFDS_GTE2
  ibufds_instq5_clk0 : IBUFDS_GTE3
    port map
    (
      O               => 	CXP4_GTH_RefClk,
      ODIV2           =>    open,
      CEB             => 	'0',
      I               => 	Q5_CLK0_GTREFCLK_PAD_P_IN,
      IB              => 	Q5_CLK0_GTREFCLK_PAD_N_IN
      );
  GTH_RefClk(36)        <= CXP4_GTH_RefClk;
  GTH_RefClk(37)        <= CXP4_GTH_RefClk;
  GTH_RefClk(38)        <= CXP4_GTH_RefClk;
  GTH_RefClk(39)        <= CXP4_GTH_RefClk;
  -- end generate;

  LOGIC_RST                             <= register_map_control.GBT_LOGIC_RESET;
  General_ctrl                          <= register_map_control.GBT_GENERAL_CTRL;

  GBT_RXSLIDE(47 downto 0)              <= register_map_control.GBT_RXSLIDE.S2312 & register_map_control.GBT_RXSLIDE.S1100
                                           & register_map_control.GBT_RXSLIDE.M2312 & register_map_control.GBT_RXSLIDE.M1100;
  GBT_TXUSRRDY(23 downto 0)             <= register_map_control.GBT_TXUSRRDY.B2312 & register_map_control.GBT_TXUSRRDY.B1100;
  GBT_RXUSRRDY(23 downto 0)             <= register_map_control.GBT_RXUSRRDY.B2312 & register_map_control.GBT_RXUSRRDY.B1100;
  GBT_GTTX_RESET(29 downto 0)           <= register_map_control.GBT_GTTX_RESET.B0503 & register_map_control.GBT_GTTX_RESET.B2312
                                           & register_map_control.GBT_GTTX_RESET.B0200 & register_map_control.GBT_GTTX_RESET.B1100;
  GBT_GTRX_RESET(23 downto 0)           <= register_map_control.GBT_GTRX_RESET.B2312 & register_map_control.GBT_GTRX_RESET.B1100;
  GBT_PLL_RESET(29 downto 0)            <= register_map_control.GBT_PLL_RESET.B0503 & register_map_control.GBT_PLL_RESET.B2312
                                           & register_map_control.GBT_PLL_RESET.B0200 & register_map_control.GBT_PLL_RESET.B1100;
  GBT_SOFT_TX_RESET(29 downto 0)        <= register_map_control.GBT_SOFT_TX_RESET.B0503 & register_map_control.GBT_SOFT_TX_RESET.B2312
                                           & register_map_control.GBT_SOFT_TX_RESET.B0200 & register_map_control.GBT_SOFT_TX_RESET.B1100;
  GBT_SOFT_RX_RESET(29 downto 0)        <= register_map_control.GBT_SOFT_RX_RESET.B0503 & register_map_control.GBT_SOFT_RX_RESET.B2312
                                           & register_map_control.GBT_SOFT_RX_RESET.B0200 & register_map_control.GBT_SOFT_RX_RESET.B1100;

  GBT_ODDEVEN(23 downto 0)              <= register_map_control.GBT_ODD_EVEN.B2312 & register_map_control.GBT_ODD_EVEN.B1100;
  GBT_TOPBOT(23 downto 0)               <= register_map_control.GBT_TOPBOT.B2312 & register_map_control.GBT_TOPBOT.B1100;
  GBT_TX_TC_DLY_VALUE1(47 downto 0)     <= register_map_control.GBT_TX_TC_DLY_VALUE1;
  GBT_TX_TC_DLY_VALUE2(47 downto 0)     <= register_map_control.GBT_TX_TC_DLY_VALUE2;
  GBT_TX_OPT(47 downto 0)               <= register_map_control.GBT_TX_OPT;
  GBT_RX_OPT(47 downto 0)               <= register_map_control.GBT_RX_OPT;
  GBT_DATA_TXFORMAT(47 downto 0)        <= register_map_control.GBT_DATA_TXFORMAT.B4724 & register_map_control.GBT_DATA_TXFORMAT.B2300;
  GBT_DATA_RXFORMAT(47 downto 0)        <= register_map_control.GBT_DATA_RXFORMAT.B4724 & register_map_control.GBT_DATA_RXFORMAT.B2300;

  GBT_TX_RESET(23 downto 0)             <= register_map_control.GBT_TX_RESET.B2312 & register_map_control.GBT_TX_RESET.B1100;
  GBT_RX_RESET(23 downto 0)             <= register_map_control.GBT_RX_RESET.B2312 & register_map_control.GBT_RX_RESET.B1100;
  GBT_TX_TC_METHOD(23 downto 0)         <= register_map_control.GBT_TX_TC_METHOD.B2312 & register_map_control.GBT_TX_TC_METHOD.B1100;
  GBT_TC_EDGE(23 downto 0)              <= register_map_control.GBT_TC_EDGE.B2312 & register_map_control.GBT_TC_EDGE.B1100;
  GBT_OUTMUX_SEL(23 downto 0)           <= register_map_control.GBT_OUTMUX_SEL.B2312 & register_map_control.GBT_OUTMUX_SEL.B1100;

  register_map_gbt_monitor.GBT_VERSION.DATE             <=  GBT_VERSION(63 downto 48);
  register_map_gbt_monitor.GBT_VERSION.GBT_VERSION      <=  GBT_VERSION(23 downto 20);
  register_map_gbt_monitor.GBT_VERSION.GTH_IP_VERSION   <=  GBT_VERSION(19 downto 16);
  register_map_gbt_monitor.GBT_VERSION.RESERVED         <=  GBT_VERSION(15 downto 3);
  register_map_gbt_monitor.GBT_VERSION.GTHREFCLK_SEL    <=  (others => GTHREFCLK_SEL);
  register_map_gbt_monitor.GBT_VERSION.RX_CLK_SEL       <=  GBT_VERSION(1 downto 1);
  register_map_gbt_monitor.GBT_VERSION.PLL_SEL          <=  GBT_VERSION(0 downto 0);

  --

  register_map_gbt_monitor.GBT_TXRESET_DONE.B1100      <= TxResetDone(11 downto 0);
  register_map_gbt_monitor.GBT_TXRESET_DONE.B2312      <= TxResetDone(23 downto 12);
  register_map_gbt_monitor.GBT_RXRESET_DONE.B1100      <= RxResetDone(11 downto 0);
  register_map_gbt_monitor.GBT_RXRESET_DONE.B2312      <= RxResetDone(23 downto 12);
  register_map_gbt_monitor.GBT_TXFSMRESET_DONE.B1100   <= txpmaresetdone(11 downto 0);
  register_map_gbt_monitor.GBT_TXFSMRESET_DONE.B2312   <= txpmaresetdone(23 downto 12);
  register_map_gbt_monitor.GBT_RXFSMRESET_DONE.B1100   <= rxpmaresetdone(11 downto 0);
  register_map_gbt_monitor.GBT_RXFSMRESET_DONE.B2312   <= rxpmaresetdone(23 downto 12);
  register_map_gbt_monitor.GBT_CPLL_FBCLK_LOST.B1100   <= CpllFbClkLost(11 downto 0);
  register_map_gbt_monitor.GBT_CPLL_FBCLK_LOST.B2312   <= CpllFbClkLost(23 downto 12);
  register_map_gbt_monitor.GBT_CPLL_LOCK.B1100         <= CpllLock(11 downto 0);
  register_map_gbt_monitor.GBT_CPLL_LOCK.B2312         <= CpllLock(23 downto 12);
  register_map_gbt_monitor.GBT_CPLL_LOCK.B0200         <= QpllLock(2 downto 0);
  register_map_gbt_monitor.GBT_CPLL_LOCK.B0503         <= QpllLock(5 downto 3);
  register_map_gbt_monitor.GBT_RXCDR_LOCK.B1100        <= RxCdrLock(11 downto 0);
  register_map_gbt_monitor.GBT_RXCDR_LOCK.B2312        <= RxCdrLock(23 downto 12);
  register_map_gbt_monitor.GBT_CLK_SAMPLED.B1100       <= clk_sampled(11 downto 0);
  register_map_gbt_monitor.GBT_CLK_SAMPLED.B2312       <= clk_sampled(23 downto 12);

  register_map_gbt_monitor.GBT_RX_IS_HEADER.B1100      <= RX_IS_HEADER(11 downto 0);
  register_map_gbt_monitor.GBT_RX_IS_HEADER.B2312      <= RX_IS_HEADER(23 downto 12);
  register_map_gbt_monitor.GBT_RX_IS_DATA.B1100        <= RX_IS_DATA(11 downto 0);
  register_map_gbt_monitor.GBT_RX_IS_DATA.B2312        <= RX_IS_DATA(23 downto 12);
  register_map_gbt_monitor.GBT_RX_HEADER_FOUND.B1100   <= RX_HEADER_FOUND(11 downto 0);
  register_map_gbt_monitor.GBT_RX_HEADER_FOUND.B2312   <= RX_HEADER_FOUND(23 downto 12);

  register_map_gbt_monitor.GBT_ALIGNMENT_DONE.B1100    <= alignment_done_f(11 downto 0);
  register_map_gbt_monitor.GBT_ALIGNMENT_DONE.B2312    <= alignment_done_f(23 downto 12);

  -- aligndone_gen : for i in 23 downto 0 generate
  --   alignment_done_f(i) <=  RxCdrLock(i) and alignment_done(i);
  -- end generate;

  register_map_gbt_monitor.GBT_OUT_MUX_STATUS.B1100    <= outsel_o(11 downto 0);
  register_map_gbt_monitor.GBT_OUT_MUX_STATUS.B2312    <= outsel_o(23 downto 12);
  register_map_gbt_monitor.GBT_ERROR.B1100             <= error_f(11 downto 0);
  register_map_gbt_monitor.GBT_ERROR.B2312             <= error_f(23 downto 12);

  error_gen : for i in 47 downto 0 generate
    error_f(i) <= error_orig(i) and alignment_done_f(i);
  end generate;

  register_map_gbt_monitor.GBT_GBT_TOPBOT_C.B1100      <= TopBot_c(11 downto 0);
  register_map_gbt_monitor.GBT_GBT_TOPBOT_C.B2312      <= TopBot_c(23 downto 12);


----------------------------------------
------ REGISTERS MAPPING
----------------------------------------
  alignment_chk_rst_i           <= General_ctrl(0);


  DESMUX_USE_SW                 <= register_map_control.GBT_MODE_CTRL.DESMUX_USE_SW(0);
  RX_ALIGN_SW                   <= register_map_control.GBT_MODE_CTRL.RX_ALIGN_SW(1);
  RX_ALIGN_TB_SW                <= register_map_control.GBT_MODE_CTRL.RX_ALIGN_TB_SW(2);


---- Bit slip register: address 0ffset + 0x80
  RxSlide_Manual(11 downto 0)   <= GBT_RXSLIDE(11 downto 0); -- Default: 0x000
  RxSlide_Manual(23 downto 12)  <= GBT_RXSLIDE(23 downto 12); -- Default: 0x000
  RxSlide_Sel(11 downto 0)      <= GBT_RXSLIDE(35 downto 24); -- Default: 0x000
  RxSlide_Sel(23 downto 12)     <= GBT_RXSLIDE(47 downto 36); -- Default: 0x000

---- Tx User Ready register: address 0ffset + 0x90
  TxUsrRdy(11 downto 0)         <= GBT_TXUSRRDY(11 downto 0); ---- Default: 0xFFF
  TxUsrRdy(23 downto 12)        <= GBT_TXUSRRDY(23 downto 12); ---- Default: 0xFFF

---- Rx User Ready register: address 0ffset + 0xA0
  RxUsrRdy(11 downto 0)         <= GBT_RXUSRRDY(11 downto 0); ---- Default: 0xFFF
  RxUsrRdy(23 downto 12)        <= GBT_RXUSRRDY(23 downto 12); ---- Default: 0xFFF

---- SOFT RESET & GTH TX RESET register: address 0ffset + 0xB0
  GTTX_RESET(11 downto 0)       <= GBT_GTTX_RESET(11 downto 0); -- Default: 0b000
  GTTX_RESET(23 downto 12)      <= GBT_GTTX_RESET(26 downto 15); -- Default: 0b000
  SOFT_RESET(2 downto 0)        <= GBT_GTTX_RESET(14 downto 12);  -- Default: 0b000
  SOFT_RESET(5 downto 3)        <= GBT_GTTX_RESET(29 downto 27);  -- Default: 0b000

---- GTH RX RESET register: address 0ffset + 0xC0
  GTRX_RESET(11 downto 0)       <= GBT_GTRX_RESET(11 downto 0); -- Default: 0b000
  GTRX_RESET(23 downto 12)      <= GBT_GTRX_RESET(23 downto 12); -- Default: 0b000

---- CPLL QPLL RESET register: address 0ffset + 0xD0
  CPLL_RESET(11 downto 0)       <= GBT_PLL_RESET(11 downto 0); -- Default: 0b000
  CPLL_RESET(23 downto 12)      <= GBT_PLL_RESET(26 downto 15); -- Default: 0b000
  QPLL_RESET(2 downto 0)        <= GBT_PLL_RESET(14 downto 12);  -- Default: 0b000
  QPLL_RESET(5 downto 3)        <= GBT_PLL_RESET(29 downto 27); -- Default: 0b000

---- SOFT TX RESET register: address 0ffset + 0xE0
  SOFT_TXRST_GT(11 downto 0)    <= GBT_SOFT_TX_RESET(11 downto 0);  -- Default: 0b000
  SOFT_TXRST_GT(23 downto 12)   <= GBT_SOFT_TX_RESET(26 downto 15); -- Default: 0b000
  SOFT_TXRST_ALL(2 downto 0)    <= GBT_SOFT_TX_RESET(14 downto 12);
  SOFT_TXRST_ALL(5 downto 3)    <= GBT_SOFT_TX_RESET(29 downto 27);

---- SOFT RX RESETregister: address 0ffset + 0xF0
  SOFT_RXRST_GT(11 downto 0)    <= GBT_SOFT_RX_RESET(11 downto 0);  -- Default: 0b000
  SOFT_RXRST_GT(23 downto 12)   <= GBT_SOFT_RX_RESET(26 downto 15); -- Default: 0b000
  SOFT_RXRST_ALL(2 downto 0)    <= GBT_SOFT_RX_RESET(14 downto 12);
  SOFT_RXRST_ALL(5 downto 3)    <= GBT_SOFT_RX_RESET(29 downto 27);

---- ODDEVEN register: address 0ffset + 0x100
  OddEven(11 downto 0)          <= GBT_ODDEVEN(11 downto 0);
  OddEven(23 downto 12)         <= GBT_ODDEVEN(23 downto 12);

---- TOPBOT register: address 0ffset + 0x110
  TopBot(23 downto 12)          <= GBT_TOPBOT(23 downto 12);
  TopBot(11 downto 0)           <= GBT_TOPBOT(11 downto 0);

-- Tx Timedomain crossing phase selection :address 0ffset + 0x120 & 0x130
  TX_TC_DLY_VALUE(47 downto 0)  <= GBT_TX_TC_DLY_VALUE1(47 DOWNTO 0);
  TX_TC_DLY_VALUE(95 downto 48) <= GBT_TX_TC_DLY_VALUE2(47 DOWNTO 0);

  --Adjust the Tx Optimization Selection
  TX_OPT(47 downto 0)           <= GBT_TX_OPT(47 DOWNTO 0);  --
  --Adjust the Rx Optimization Selection
  RX_OPT(47 downto 0)           <= GBT_RX_OPT(47 DOWNTO 0);  --
  -- GBT-FRAME, WIDEBUS, FELIX_8B10B for Tx
  DATA_TXFORMAT(23 downto 0)    <= GBT_DATA_TXFORMAT(23 DOWNTO 0);  --
  DATA_TXFORMAT(47 downto 24)   <= GBT_DATA_TXFORMAT(47 DOWNTO 24);  --
  -- GBT-FRAME, WIDEBUS, FELIX_8B10B for Rx
  DATA_RXFORMAT(23 downto 0)    <= GBT_DATA_RXFORMAT(23 DOWNTO 0);  --
  DATA_RXFORMAT(47 downto 24)   <= GBT_DATA_RXFORMAT(47 DOWNTO 24);  --

  -- GBT Tx LOGIC RESET
  TX_RESET(11 downto 0)         <= GBT_TX_RESET(11 DOWNTO 0);
  TX_RESET(23 downto 12)        <= GBT_TX_RESET(23 DOWNTO 12);
  -- GBT Rx LOGIC RESET
  RX_RESET(11 downto 0)         <= GBT_RX_RESET(11 DOWNTO 0);
  RX_RESET(23 downto 12)        <= GBT_RX_RESET(23 DOWNTO 12);


  -- Tx time domain crossing method sel
  TX_TC_METHOD(11 downto 0)     <= GBT_TX_TC_METHOD(11 DOWNTO 0);
  TX_TC_METHOD(23 downto 12)    <= GBT_TX_TC_METHOD(23 DOWNTO 12);
  -- Tx time domain crossing edge sample edge sel
  TC_EDGE(11 downto 0)          <= GBT_TC_EDGE(11 DOWNTO 0);
  TC_EDGE(23 downto 12)         <= GBT_TC_EDGE(23 DOWNTO 12);
  -- descrambler output MUX selection
  outsel_i(11 downto 0)         <= GBT_OUTMUX_SEL(11 DOWNTO 0);
  outsel_i(23 downto 12)        <= GBT_OUTMUX_SEL(23 DOWNTO 12);

  -------

  datamod_gen1 : if DYNAMIC_DATA_MODE_EN='1' generate
    DATA_TXFORMAT_i <= DATA_TXFORMAT;
    DATA_RXFORMAT_i <= DATA_RXFORMAT;
  end generate;

  datamod_gen2 : if DYNAMIC_DATA_MODE_EN='0' generate
    DATA_TXFORMAT_i <= GBT_DATA_TXFORMAT_PACKAGE;
    DATA_RXFORMAT_i <= GBT_DATA_RXFORMAT_PACKAGE;
  end generate;

  process(clk40_in)
  begin
    if clk40_in'event and clk40_in='1' then
      pulse_lg <= pulse_cnt(20);
      if pulse_cnt(20)='1' then
        pulse_cnt <=(others=>'0');
      else
        pulse_cnt <= pulse_cnt+'1';
      end if;
    end if;
  end process;

  process(clk40_in)
  begin
    if clk40_in'event and clk40_in='1' then
      alignment_done_chk_cnt <= alignment_done_chk_cnt + '1';
    end if;
  end process;

  rxalign_auto : for i in GBT_NUM-1 downto 0 generate

--  process(clk40_in)
--  begin
--    if clk40_in'event and clk40_in='1' then
--      if pulse_lg = '1' then
--        gbt_sel(i) <= lock_lg(i);
--      end if;
--      if  pulse_lg = '1' then
--        lock_lg(i) <='1';
--      elsif alignment_done_f(i)='0' then
--        lock_lg(i) <='0';
--      end if;
--    end if;
--  end process;

    process(clk40_in)
    begin
      if clk40_in'event and clk40_in='1' then
        if alignment_done_chk_cnt="0000000000000" then
          alignment_done_a(i) <= alignment_done(i);
        else
          alignment_done_a(i) <= alignment_done(i) and alignment_done_a(i);
        end if;
        if alignment_done_chk_cnt="1000000000000" then
          alignment_done_f(i) <=  RxCdrLock(i) and alignment_done_a(i);
        end if;
      end if;
    end process;


    RX_120b_out(i) <= RX_120b_out_ii(i) when alignment_done_f(i)='1'
                      else (others =>'0');

    auto_rxrst : entity work.FELIX_GBT_RX_AUTO_RST
      port map
      (
        FSM_CLK                 => clk40_in,
        pulse_lg                => pulse_lg,
        GTHRXRESET_DONE         => RxResetDone(i),-- and RxFsmResetDone(i),
        alignment_chk_rst       => alignment_chk_rst_c1(i),
        GBT_LOCK                => alignment_done_f(i),--alignment_done(i),
        AUTO_GTH_RXRST          => auto_gth_rxrst(i),
        ext_trig_realign        => ext_trig_realign(i),
        AUTO_GBT_RXRST          => auto_gbt_rxrst(i)
        );

    rafsm : entity work.FELIX_GBT_RXSLIDE_FSM
      port map
      (
        ext_trig_realign        => ext_trig_realign(i),
        FSM_RST                 => FSM_RST(i),
        FSM_CLK                 => clk40_in,
        GBT_LOCK                => alignment_done(i),
        RxSlide                 => RxSlide_c(i),
        alignment_chk_rst       => alignment_chk_rst_c(i)
        );

    FSM_RST(i)          <= RX_RESET(i);-- or RX_ALIGN_SW;
 -- GTRX_RESET_i(i)     <= --GTRX_RESET(i) when RX_ALIGN_SW='1' else
 --                      (GTRX_RESET(i) or auto_gth_rxrst(i));
    RX_RESET_i(i)       <= --RX_RESET(i) when RX_ALIGN_SW='1' else
                           (RX_RESET(i) or auto_gbt_rxrst(i));
    alignment_chk_rst(i)        <= --alignment_chk_rst_i when RX_ALIGN_SW='1' else
                                   (alignment_chk_rst_i or alignment_chk_rst_c(i) or alignment_chk_rst_c1(i));

    TX_RESET_i(i)       <= TX_RESET(i) or (not TxResetDone(i));-- or (not TxFsmResetDone(i));
  end generate;

  outsel_ii             <= outsel_o when DESMUX_USE_SW = '0' else
                           outsel_i;

--  OddEven_i           <= OddEven_c when RX_ALIGN_SW ='0' else
--                      OddEven;

--  TopBot_i            <= TopBot_c when RX_ALIGN_SW='0' else --and RX_ALIGN_TB_SW='0'  else
--                      TopBot;

  RxSlide_i             <= RxSlide_c;-- when RX_ALIGN_SW='0' else
 --                     RxSlide_Manual;

  RX_FLAG_O             <= RX_FLAG_Oi(GBT_NUM-1 downto 0);

  gbtRxTx : for i in GBT_NUM-1 downto 0 generate
    process(GT_RX_WORD_CLK(i))
    begin
      if GT_RX_WORD_CLK(i)'event and GT_RX_WORD_CLK(i)='1' then
        BITSLIP_MANUAL_r(i)     <= RxSlide_i(i);
        BITSLIP_MANUAL_2r(i)    <= BITSLIP_MANUAL_r(i);
        BITSLIP_MANUAL_3r(i)    <= BITSLIP_MANUAL_2r(i);
        RxSlide(i)              <= BITSLIP_MANUAL_r(i) and (not BITSLIP_MANUAL_2r(i));
      end if;
    end process;


    alignment_chk_rst_f(i)      <= alignment_chk_rst(i);-- or (not RxCdrLock(i));
    gbtTxRx_inst: entity work.gbtTxRx_FELIX
      generic map
      (
        channel => i
        )
      port map
      (
        error_o                 => error_orig(i),
        RX_FLAG                 => RX_FLAG_Oi(i),--RX_FLAG_O(i),
        TX_FLAG                 => TX_FLAG_O(i),

        Tx_DATA_FORMAT          => DATA_TXFORMAT_i(2*i+1 downto 2*i),
        Rx_DATA_FORMAT          => DATA_RXFORMAT_i(2*i+1 downto 2*i),

        Tx_latopt_tc            => TX_OPT(i),
        Tx_latopt_scr           => TX_OPT(24+i),
        RX_LATOPT_DES           => RX_OPT(i),

        TX_TC_METHOD            => TX_TC_METHOD(i),
        TC_EDGE                 => TC_EDGE(i),
        TX_TC_DLY_VALUE  	=> TX_TC_DLY_VALUE(4*i+2 downto 4*i),

        alignment_chk_rst       => alignment_chk_rst_f(i),
        alignment_done_O        => alignment_done(i),
        L40M                    => clk40_in,
        outsel_i                => outsel_ii(i),
        outsel_o                => outsel_o(i),

        --BITSLIP_MANUAL        => RxSlide_i(i),
        --BITSLIP_SEL 	        => RxSlide_Sel(i),
        --GT_RXSLIDE		=> RxSlide(i),
        OddEven			=> '0',--OddEven_i(i),
        TopBot                  => '0',--TopBot_i(i),
        data_sel                => data_sel(4*i+3 downto 4*i),

        TX_RESET_I 		=> TX_RESET_i(i),
        TX_FRAMECLK_I	        => TX_FRAME_CLK_I(i),
        TX_WORDCLK_I 	        => GT_TX_WORD_CLK(i),
        --TX_ISDATA_SEL_I	=> TX_IS_DATA(i),
        TX_DATA_120b_I	        => TX_120b_in(i),
        TX_DATA_20b_O	        => TX_DATA_20b(i),

        RX_RESET_I  		=> RX_RESET_i(i),
        RX_FRAME_CLK_O 		=> open,--RX_FRAME_CLK_O(i),
        RX_WORD_IS_HEADER_O     => RX_IS_HEADER(i),
        RX_HEADER_FOUND	        => RX_HEADER_FOUND(i),
        RX_ISDATA_FLAG_O        => RX_IS_DATA(i),
        RX_DATA_20b_I    	=> RX_DATA_20b(i),
        RX_DATA_120b_O    	=> RX_120b_out_i(i),
        des_rxusrclk            => GT_RX_WORD_CLK(i),
        RX_WORDCLK_I      	=> GT_RX_WORD_CLK(i)

        );

    fifo_inst: fifo_GBT2CR
      PORT MAP(
        rd_rst          => rst_hw,
        wr_rst          => rst_hw,
        wr_clk          => GT_RX_WORD_CLK(i),
        rd_clk          => clk40_in,--FIFO_RD_CLK(i),
        din             => RX_120b_out_i(i),
        wr_en           => RX_FLAG_Oi(i),
        rd_en           => not fifo_empty(i),--'1',--FIFO_RD_EN(i),
        dout            => RX_120b_out_ii(i),
        full            => open,
        empty           => open,
        prog_empty      => fifo_empty(i)--FIFO_EMPTY(i)
        );

  end generate;





-------------------------------
------ GTH TOP WRAPPER
-------------------------------

  clk_generate : for i in GBT_NUM-1 downto 0 generate

    GTTXOUTCLK_BUFG: bufg_gt
      port map(
        i       => GT_TXOUTCLK(i),
        div     => "000",
        clr     => '0',--userclk_tx_reset_in,--'0',
        cemask  => '0',
        clrmask => '0',
        ce      => '1',
        o       => GT_TXUSRCLK(i)
        );

    GT_TX_WORD_CLK(i) <= GT_TXUSRCLK(i);

    GTRXOUTCLK_BUFG: bufg_gt
      port map(
        i       => GT_RXOUTCLK(i),
        div     => "000",
        clr     => '0',--userclk_tx_reset_in,--'0',
        cemask  => '0',
        clrmask => '0',
        ce      => '1',
        o       => GT_RXUSRCLK(i)
        );

    -- GT_RXUSRCLK(i) <=  clk240_in;

    GT_RX_WORD_CLK(i) <= GT_RXUSRCLK(i);

  end generate;



 QPLL_GEN: if PLL_SEL = QPLL generate
   port_trans : for i in GBT_NUM-1 downto 0 generate
   RX_N_i(i) <=RX_N(i);
   RX_P_i(i) <=RX_P(i);
   TX_N(i) <=TX_N_i(i);
   TX_P(i) <=TX_P_i(i);   
   
   end generate;
   GTH_inst : for i in (GBT_NUM-1)/4 downto 0 generate

     RX_DATA_20b(4*i+0) <=RX_DATA_80b(i)(19 downto 0);
     RX_DATA_20b(4*i+1) <=RX_DATA_80b(i)(39 downto 20);
     RX_DATA_20b(4*i+2) <=RX_DATA_80b(i)(59 downto 40);
     RX_DATA_20b(4*i+3) <=RX_DATA_80b(i)(79 downto 60);

    GTH_TOP_INST: entity work.GTH_QPLL_Wrapper
      Port map(
        gthrxn_in                       => RX_N_i(4*i+3 downto 4*i),
        gthrxp_in                       => RX_P_i(4*i+3 downto 4*i),
        gthtxn_out                      => TX_N_i(4*i+3 downto 4*i),
        gthtxp_out                      => TX_P_i(4*i+3 downto 4*i),

        drpclk_in                       => (others=>clk40_in),
        gtrefclk0_in                    => GTH_RefClk(4*i downto 4*i),
        gt_rxusrclk_in                  => GT_RX_WORD_CLK(4*i+3 downto 4*i),
        gt_rxoutclk_out                 => GT_RXOUTCLK(4*i+3 downto 4*i),
        gt_txusrclk_in                  => GT_TX_WORD_CLK(4*i+3 downto 4*i),
        gt_txoutclk_out                 => GT_TXOUTCLK(4*i+3 downto 4*i),

        userdata_tx_in                  =>  TX_DATA_20b(4*i+3) & TX_DATA_20b(4*i+2) & TX_DATA_20b(4*i+1) & TX_DATA_20b(4*i+0),
        userdata_rx_out                 =>  RX_DATA_80b(i),
        rxpolarity_in                   => register_map_control.GBT_TRANSCEIVER_POLARITY.RXPOLARITY(4*i+3 downto 4*i),
        txpolarity_in                   => register_map_control.GBT_TRANSCEIVER_POLARITY.TXPOLARITY(4*i+27 downto 4*i + 24),


        -- for loopback: default, both signal need to be all '0'
        -- read UG476 for the details. NOTE: the TXBUFFER is disabled, so some type of loopbhack may be
        -- not supported.
        loopback_in                     => "000",--register_map_control.GTH_LOOPBACK_CONTROL,
        rxcdrhold_in                    => '0',

        userclk_rx_reset_in             => userclk_rx_reset_in(i downto i),--(others=>(not rxpmaresetdone_out(i))),--locked,
        userclk_tx_reset_in             => userclk_tx_reset_in(i downto i),--(others=>(not txpmaresetdone_out(i))),--,--locked,

        -- reset_clk_freerun_in                    : in std_logic_vector(0 downto 0);
        reset_all_in                           => SOFT_RESET_f(i downto i),
        reset_tx_pll_and_datapath_in           => QPLL_RESET(i downto i),
        reset_tx_datapath_in                   => GTTX_RESET_MERGE(i downto i),
        reset_rx_pll_and_datapath_in           => QPLL_RESET(i downto i),
        reset_rx_datapath_in                   => GTRX_RESET_MERGE(i downto i),

        qpll0lock_out                          => open,
        qpll1lock_out                          => QpllLock(i downto i),
        qpll1fbclklost_out                     => open,--
        qpll0fbclklost_out                     => open,
        rxslide_in                             => RxSlide(4*i+3 downto 4*i),

        rxresetdone_out                         => rxresetdone(4*i+3 downto 4*i),
        txresetdone_out                         => txresetdone(4*i+3 downto 4*i),
        rxpmaresetdone_out                      => rxpmaresetdone(4*i+3 downto 4*i),
        txpmaresetdone_out                      => txpmaresetdone(4*i+3 downto 4*i),
        reset_tx_done_out                       => txresetdone_quad(i downto i),
        reset_rx_done_out                       => rxresetdone_quad(i downto i),
        reset_rx_cdr_stable_out                 => RxCdrLock_quad(i downto i),
        rxcdrlock_out                           => rxcdrlock_out(4*i+3 downto 4*i)
        );


     process(clk40_in)
     begin
       if clk40_in'event and clk40_in='1' then
         if cdr_cnt ="000000000" then
           RxCdrLock_a(4*i)     <= rxcdrlock_out(4*i);
           RxCdrLock_a(4*i+1)   <= rxcdrlock_out(4*i+1);
           RxCdrLock_a(4*i+2)   <= rxcdrlock_out(4*i+2);
           RxCdrLock_a(4*i+3)   <= rxcdrlock_out(4*i+3);
         else
           RxCdrLock_a(4*i) <= RxCdrLock_a(4*i) and rxcdrlock_out(4*i);
           RxCdrLock_a(4*i+1) <= RxCdrLock_a(4*i+1) and rxcdrlock_out(4*i+1);
           RxCdrLock_a(4*i+2) <= RxCdrLock_a(4*i+2) and rxcdrlock_out(4*i+2);
           RxCdrLock_a(4*i+3) <= RxCdrLock_a(4*i+3) and rxcdrlock_out(4*i+3);
         end if;
         if cdr_cnt="100000000" then
           RxCdrLock(4*i) <=RxCdrLock_a(4*i);
           RxCdrLock(4*i+1) <=RxCdrLock_a(4*i+1);
           RxCdrLock(4*i+2) <=RxCdrLock_a(4*i+2);
           RxCdrLock(4*i+3) <=RxCdrLock_a(4*i+3);
         end if;
       end if;
     end process;

     SOFT_RESET_f(i) <= SOFT_RESET(i) or rst_hw or QPLL_RESET(i);-- or GTRX_RESET(i);

     userclk_rx_reset_in(i) <=not (rxpmaresetdone(4*i+0) or rxpmaresetdone(4*i+1) or rxpmaresetdone(4*i+2) or rxpmaresetdone(4*i+3));
     userclk_tx_reset_in(i) <=not (txpmaresetdone(4*i+0) or txpmaresetdone(4*i+1) or txpmaresetdone(4*i+2) or txpmaresetdone(4*i+3));

     GTTX_RESET_MERGE(i) <= GTTX_RESET(4*i) or GTTX_RESET(4*i+1) or GTTX_RESET(4*i+2) or GTTX_RESET(4*i+3);
     GTRX_RESET_MERGE(i) <= (GTRX_RESET(4*i) or (auto_gth_rxrst(4*i) and RxCdrLock(4*i)))
                            or (GTRX_RESET(4*i+1) or (auto_gth_rxrst(4*i+1) and RxCdrLock(4*i+1)))
                            or (GTRX_RESET(4*i+2) or (auto_gth_rxrst(4*i+2) and RxCdrLock(4*i+2)))
                            or (GTRX_RESET(4*i+3) or (auto_gth_rxrst(4*i+3) and RxCdrLock(4*i+3))) ;
     --GTRX_RESET_MERGE(i) <= GTRX_RESET(4*i) or GTRX_RESET(4*i+1) or GTRX_RESET(4*i+2) or GTRX_RESET(4*i+3);

     -- CpllLock(i) <= '1';

   end generate;
 end generate;


 process(clk40_in)
  begin
  if clk40_in'event and clk40_in='1' then
   cdr_cnt <=cdr_cnt+'1';
  end if;
  end process;


 CPLL_GEN: if  PLL_SEL = CPLL generate
   GTH_inst : for i in GBT_NUM-1 downto 0 generate

     GTH_TOP_INST: entity work.GTH_CPLL_Wrapper
       Port map(
         gthrxn_in                              => RX_N(i downto i),
         gthrxp_in                              => RX_P(i downto i),
         gthtxn_out                             => TX_N(i downto i),
         gthtxp_out                             => TX_P(i downto i),
         drpclk_in                              => (others=>clk40_in),
         gtrefclk0_in                           => GTH_RefClk(i downto i),

         gt0_rxusrclk_in                        => GT_RX_WORD_CLK(i downto i),
         gt0_rxoutclk_out                       => GT_RXOUTCLK(i downto i),
         gt0_txusrclk_in                        => GT_TX_WORD_CLK(i downto i),
         gt0_txoutclk_out                       => GT_TXOUTCLK(i downto i),

         userdata_tx_in                         =>  TX_DATA_20b(i),
         userdata_rx_out                        =>  RX_DATA_20b(i),
         rxpolarity_in                          => register_map_control.GBT_TRANSCEIVER_POLARITY.RXPOLARITY(i downto i),
         txpolarity_in                          => register_map_control.GBT_TRANSCEIVER_POLARITY.TXPOLARITY(i + 24 downto i + 24),

         -- for loopback: default, both signal need to be all '0'
         -- read UG476 for the details. NOTE: the TXBUFFER is disabled, so some type of loopbhack may be
         -- not supported.
         loopback_in                            => "000",--register_map_control.GTH_LOOPBACK_CONTROL,
         rxcdrhold_in                           => '0',


         userclk_rx_reset_in                    => userclk_rx_reset_in(i downto i),--(others=>(not rxpmaresetdone_out(i))),--locked,
         userclk_tx_reset_in                    => userclk_tx_reset_in(i downto i),--(others=>(not txpmaresetdone_out(i))),--,--locked,

         -- reset_clk_freerun_in                    : in std_logic_vector(0 downto 0);
         reset_all_in                           => SOFT_RESET_f(i downto i),
         reset_tx_pll_and_datapath_in           => CPLL_RESET(i downto i),
         reset_tx_datapath_in                   => GTTX_RESET(i downto i),
         reset_rx_pll_and_datapath_in           => CPLL_RESET(i downto i),
         reset_rx_datapath_in                   => GTRX_RESET_i(i downto i),-- and RxCdrLock(i downto i),


         cpllfbclklost_out                      => cpllfbclklost(i downto i),
         cplllock_out                           => cplllock(i downto i),


         rxslide_in                             => RxSlide(i downto i),



         rxpmaresetdone_out                     => rxpmaresetdone(i downto i),
         txpmaresetdone_out                     => txpmaresetdone(i downto i),

         reset_tx_done_out                      => txresetdone(i downto i),
         reset_rx_done_out                      => rxresetdone(i downto i),
         reset_rx_cdr_stable_out                => RxCdrLock(i downto i)

         );



     GTRX_RESET_i(i) <= --GTRX_RESET(i) when RX_ALIGN_SW='1' else
                       GTRX_RESET(i) or (auto_gth_rxrst(i) and RxCdrLock(i));

     SOFT_RESET_f(i) <= SOFT_RESET(i/4) or rst_hw or CPLL_RESET(i);-- or GTRX_RESET(i);

     userclk_rx_reset_in(i) <=not rxpmaresetdone(i);
     userclk_tx_reset_in(i) <=not txpmaresetdone(i);
     --RxResetDone_f(i) <= RxResetDone(i);

   end generate;
 end generate;


end Behavioral;
