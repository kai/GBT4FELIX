##GBT Transceiver pins
#bank 128
set_property PACKAGE_PIN AC43 [get_ports {RX_P[0]}]
set_property PACKAGE_PIN AF41 [get_ports {RX_P[3]}]
set_property PACKAGE_PIN AE43 [get_ports {RX_P[2]}]
set_property PACKAGE_PIN AG43 [get_ports {RX_P[1]}]
#bank 127
set_property PACKAGE_PIN AH41 [get_ports {RX_P[4]}]
set_property PACKAGE_PIN AJ43 [get_ports {RX_P[5]}]
set_property PACKAGE_PIN AN43 [get_ports {RX_P[6]}]
set_property PACKAGE_PIN AL43 [get_ports {RX_P[7]}]
#bank 126
set_property PACKAGE_PIN AU43 [get_ports {RX_P[8]}]
set_property PACKAGE_PIN AR43 [get_ports {RX_P[9]}]
set_property PACKAGE_PIN BA43 [get_ports {RX_P[10]}]
set_property PACKAGE_PIN AW43 [get_ports {RX_P[11]}]
#bank 133
set_property PACKAGE_PIN C43  [get_ports {RX_P[12]}]
set_property PACKAGE_PIN E43  [get_ports {RX_P[13]}]
set_property PACKAGE_PIN G43  [get_ports {RX_P[14]}]
set_property PACKAGE_PIN J43  [get_ports {RX_P[15]}]
#bank 132
set_property PACKAGE_PIN L43  [get_ports {RX_P[16]}]
set_property PACKAGE_PIN N43  [get_ports {RX_P[17]}]
set_property PACKAGE_PIN R43  [get_ports {RX_P[18]}]
set_property PACKAGE_PIN U43  [get_ports {RX_P[19]}]
#bank 131
set_property PACKAGE_PIN W43  [get_ports {RX_P[20]}]
set_property PACKAGE_PIN V41  [get_ports {RX_P[21]}]
set_property PACKAGE_PIN AA43 [get_ports {RX_P[23]}]
set_property PACKAGE_PIN AB41 [get_ports {RX_P[22]}]
#MGT0,1,2,4 bank 128, use clk from bank 127 AH37
#MGT3,5,6,7 bank 127, use clk from bank 127 AH37
#MGT8k,9,10,11 bank 126, use clk from bank 127 AH37
#MGT12,13,14,15 bank 133, use clk from bank 132 T37
