--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: V7 CPLL GTH Wrapper  (Tx Low Latency, RX with inside buffer)
-- Module Name: GTH_CPLL_Wrapper_V7 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE: V7 CPLL GTH (for GBT)
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;


--***************************** Entity Declaration ****************************
entity GTH_CPLL_Wrapper_V7 is

port
(
  GTH_RefClk	                        : in   std_logic; 
  DRP_CLK_IN                            : in   std_logic;
   
  gt0_loopback_in                       : in   std_logic_vector(2 downto 0);
  gt0_rxcdrhold_in                      : in   std_logic;
    
  --- RX clock, for each channel
  gt0_rxusrclk_in                       : in   std_logic;
  gt0_rxoutclk_out                      : out  std_logic;
  gt0_txusrclk_in                       : in   std_logic;
  gt0_txoutclk_out                      : out  std_logic;

        
-----------------------------------------
---- STATUS signals
-----------------------------------------
  gt0_txresetdone_out                   : out  std_logic; 
  gt0_rxresetdone_out                   : out  std_logic;
    
  gt0_tx_fsm_reset_done_out             : out  std_logic;
  gt0_rx_fsm_reset_done_out             : out  std_logic;
    
  gt0_cpllfbclklost_out                 : out  std_logic;
  gt0_cplllock_out                      : out  std_logic;
    
  gt0_rxcdrlock_out                     : out  std_logic;
  --gt_qplllock_out                     : out  std_logic;
---------------------------
---- CTRL signals
---------------------------
  gt0_rxslide_in                        : in   std_logic;
  gt_txpolarity_in                      : in   std_logic;
  gt_rxpolarity_in                      : in   std_logic;
  gt0_txuserrdy_in                      : in   std_logic; 
  gt0_rxuserrdy_in                      : in   std_logic;
    
----------------------------------------------------------------
----------RESET SIGNALs
----------------------------------------------------------------     
    
  SOFT_RESET_IN                         : in   std_logic; 
  GTTX_RESET_IN                         : in   std_logic;
  GTRX_RESET_IN                         : in   std_logic;
  gt0_cpllreset_in                      : in   std_logic;
  --QPLL_RESET_IN                       : in   std_logic;
   
  SOFT_TXRST_GT                         : in   std_logic;
  SOFT_RXRST_GT                         : in   std_logic;
  --SOFT_TXRST_ALL                      : in   std_logic;
  --SOFT_RXRST_ALL                      : in   std_logic;

-----------------------------------------------------------
----------- Data and TX/RX Ports
-----------------------------------------------------------
    
  gt0_txdata_in                         : in std_logic_vector(19 downto 0);
  gt0_rxdata_out                        : out std_logic_vector(19 downto 0);
   
    
  gt0_gthrxn_in                         : in   std_logic;
  gt0_gthrxp_in                         : in   std_logic;
  gt0_gthtxn_out                        : out  std_logic;
  gt0_gthtxp_out                        : out  std_logic
   
   
   

);
end GTH_CPLL_Wrapper_V7;

architecture RTL of GTH_CPLL_Wrapper_V7 is
    attribute DowngradeIPIdentifiedWarnings : string;
    attribute DowngradeIPIdentifiedWarnings of RTL : architecture is "yes";

    attribute X_CORE_INFO : string;
    attribute X_CORE_INFO of RTL : architecture is "gtwizard_qpll_4p8g_4ch,gtwizard_v3_4,{protocol_file=Start_from_scratch}";
    attribute CORE_GENERATION_INFO : string;
    attribute CORE_GENERATION_INFO of RTL : architecture is "gtwizard_qpll_4p8g_4ch,gtwizard_v3_4,{protocol_file=Start_from_scratch}";

--**************************Component Declarations*****************************


component gtwizard_CPLL_4p8g_V7
 
port
(
    SYSCLK_IN                               : in   std_logic;
    SOFT_RESET_TX_IN                        : in   std_logic;
    SOFT_RESET_RX_IN                        : in   std_logic;
    DONT_RESET_ON_DATA_ERROR_IN             : in   std_logic;
    GT0_TX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_RX_FSM_RESET_DONE_OUT               : out  std_logic;
    GT0_DATA_VALID_IN                       : in   std_logic;

    --_________________________________________________________________________
    --GT0  (X1Y4)
    --____________________________CHANNEL PORTS________________________________
    --------------------------------- CPLL Ports -------------------------------
    gt0_cpllfbclklost_out                   : out  std_logic;
    gt0_cplllock_out                        : out  std_logic;
    gt0_cplllockdetclk_in                   : in   std_logic;
    gt0_cpllreset_in                        : in   std_logic;
    -------------------------- Channel - Clocking Ports ------------------------
    gt0_gtrefclk0_in                        : in   std_logic;
    gt0_gtrefclk1_in                        : in   std_logic;
    ---------------------------- Channel - DRP Ports  --------------------------
    gt0_drpaddr_in                          : in   std_logic_vector(8 downto 0);
    gt0_drpclk_in                           : in   std_logic;
    gt0_drpdi_in                            : in   std_logic_vector(15 downto 0);
    gt0_drpdo_out                           : out  std_logic_vector(15 downto 0);
    gt0_drpen_in                            : in   std_logic;
    gt0_drprdy_out                          : out  std_logic;
    gt0_drpwe_in                            : in   std_logic;
    --------------------- RX Initialization and Reset Ports --------------------
    gt0_eyescanreset_in                     : in   std_logic;
    gt0_rxuserrdy_in                        : in   std_logic;
    -------------------------- RX Margin Analysis Ports ------------------------
    gt0_eyescandataerror_out                : out  std_logic;
    gt0_eyescantrigger_in                   : in   std_logic;
    --------------- Receive Ports - Comma Detection and Alignment --------------
    gt0_rxslide_in ,gt0_txpolarity_in,gt0_rxpolarity_in                         : in   std_logic;
    ------------------- Receive Ports - Digital Monitor Ports ------------------
    gt0_dmonitorout_out                     : out  std_logic_vector(14 downto 0);
    ------------------ Receive Ports - FPGA RX Interface Ports -----------------
    gt0_rxusrclk_in                         : in   std_logic;
    gt0_rxusrclk2_in                        : in   std_logic;
    ------------------ Receive Ports - FPGA RX interface Ports -----------------
    gt0_rxdata_out                          : out  std_logic_vector(19 downto 0);
    ------------------------ Receive Ports - RX AFE Ports ----------------------
    gt0_gthrxn_in                           : in   std_logic;
    --------------------- Receive Ports - RX Equalizer Ports -------------------
    gt0_rxmonitorout_out                    : out  std_logic_vector(6 downto 0);
    gt0_rxmonitorsel_in                     : in   std_logic_vector(1 downto 0);
    --------------- Receive Ports - RX Fabric Output Control Ports -------------
    gt0_rxoutclk_out                        : out  std_logic;
    gt0_rxoutclkfabric_out                  : out  std_logic;
    ------------- Receive Ports - RX Initialization and Reset Ports ------------
    gt0_gtrxreset_in                        : in   std_logic;
    ------------------------ Receive Ports -RX AFE Ports -----------------------
    gt0_gthrxp_in                           : in   std_logic;
    -------------- Receive Ports -RX Initialization and Reset Ports ------------
    gt0_rxresetdone_out                     : out  std_logic;
    --------------------- TX Initialization and Reset Ports --------------------
    gt0_gttxreset_in                        : in   std_logic;
    gt0_txuserrdy_in                        : in   std_logic;
    ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
    gt0_txusrclk_in                         : in   std_logic;
    gt0_txusrclk2_in                        : in   std_logic;
    ------------------ Transmit Ports - TX Data Path interface -----------------
    gt0_txdata_in                           : in   std_logic_vector(19 downto 0);
    ---------------- Transmit Ports - TX Driver and OOB signaling --------------
    gt0_gthtxn_out                          : out  std_logic;
    gt0_gthtxp_out                          : out  std_logic;
    ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
    gt0_txoutclk_out                        : out  std_logic;
    gt0_txoutclkfabric_out                  : out  std_logic;
    gt0_txoutclkpcs_out                     : out  std_logic;
    ------------- Transmit Ports - TX Initialization and Reset Ports -----------
    gt0_txresetdone_out                     : out  std_logic;

    gt0_loopback_in                         : in   std_logic_vector(2 downto 0);
    gt0_rxcdrhold_in                        : in   std_logic;
    --____________________________COMMON PORTS________________________________
    GT0_QPLLOUTCLK_IN                           : in std_logic;
    GT0_QPLLOUTREFCLK_IN                        : in std_logic

);

end component;



signal  tied_to_ground_i                : std_logic;
signal  gt0_cplllock_i                  : std_logic;
signal  tied_to_ground_vec_i            : std_logic_vector(63 downto 0);
signal  tied_to_vcc_i                   : std_logic;
signal  tied_to_vcc_vec_i               : std_logic_vector(7 downto 0);
    
signal gt0_drpaddr_in                   : std_logic_vector(8 downto 0):="000000000";  
signal gt0_drpdi_in                     : std_logic_vector(15 downto 0):=x"0000";  
signal gt0_drpdo_out                    : std_logic_vector(15 downto 0);
signal gt0_drpen_in                     : std_logic:='0';
signal gt0_drprdy_out                   : std_logic;
signal gt0_drpwe_in                     : std_logic:='0';
  
signal GT0_QPLLREFCLKLOST_I             : std_logic;
signal GT0_QPLLRESET_I                  : std_logic;
signal GT0_QPLLOUTREFCLK_I              : std_logic;
signal gt0_gttxreset_in                 : std_logic;
signal gt0_gtrxreset_in                 : std_logic;

 
--**************************** Main Body of Code *******************************
begin

  tied_to_ground_i                             <= '0';
  tied_to_ground_vec_i                         <= x"0000000000000000";
  tied_to_vcc_i                                <= '1';
  tied_to_vcc_vec_i                            <= x"ff";


    
   
  gt0_cplllock_out      <= gt0_cplllock_i;
  gt0_gttxreset_in      <= GTTX_RESET_IN or (not gt0_cplllock_i);
  
  gt0_gtrxreset_in      <= GTRX_RESET_IN or (not gt0_cplllock_i);
  gt0_rxcdrlock_out     <= '1';

  gtwizard_CPLL_4p8g_V7_init_i : gtwizard_CPLL_4p8g_V7
    port map
    (
      sysclk_in                       =>      DRP_CLK_IN,--sysclk_in_i,
      soft_reset_tx_in                =>      SOFT_TXRST_GT,--SOFT_RESET_TX_IN,
      soft_reset_rx_in                =>      SOFT_RXRST_GT,--SOFT_RESET_RX_IN,
      dont_reset_on_data_error_in     =>      '1',--DONT_RESET_ON_DATA_ERROR_IN,
      gt0_tx_fsm_reset_done_out       =>      gt0_tx_fsm_reset_done_out,
      gt0_rx_fsm_reset_done_out       =>      gt0_rx_fsm_reset_done_out,
      gt0_data_valid_in               =>      '1',--gt0_data_valid_in,

        --_____________________________________________________________________
        --_____________________________________________________________________
        --GT0  (X1Y4)

      gt0_txpolarity_in                 => gt_txpolarity_in,
      gt0_rxpolarity_in                 => gt_rxpolarity_in,


        --------------------------------- CPLL Ports -------------------------------
      gt0_cpllfbclklost_out           =>      gt0_cpllfbclklost_out,
      gt0_cplllock_out                =>      gt0_cplllock_i,
      gt0_cplllockdetclk_in           =>      DRP_CLK_IN,--sysclk_in_i,
      gt0_cpllreset_in                =>      gt0_cpllreset_in,
      -------------------------- Channel - Clocking Ports ------------------------
      gt0_gtrefclk0_in                =>      tied_to_ground_i,
      gt0_gtrefclk1_in                =>      GTH_RefClk,
      ---------------------------- Channel - DRP Ports  --------------------------
        
        
      gt0_drpaddr_in                  =>      gt0_drpaddr_in,
      gt0_drpclk_in                   =>      DRP_CLK_IN,
      gt0_drpdi_in                    =>      gt0_drpdi_in,
      gt0_drpdo_out                   =>      gt0_drpdo_out,
      gt0_drpen_in                    =>      gt0_drpen_in,
      gt0_drprdy_out                  =>      gt0_drprdy_out,
      gt0_drpwe_in                    =>      gt0_drpwe_in,
      --------------------- RX Initialization and Reset Ports --------------------
      gt0_eyescanreset_in             =>      '0',
      gt0_rxuserrdy_in                =>      gt0_rxuserrdy_in,
      -------------------------- RX Margin Analysis Ports ------------------------
      gt0_eyescandataerror_out        =>      open,
      gt0_eyescantrigger_in           =>      '0',
      --------------- Receive Ports - Comma Detection and Alignment --------------
      gt0_rxslide_in                  =>      gt0_rxslide_in,
      ------------------- Receive Ports - Digital Monitor Ports ------------------
      gt0_dmonitorout_out             =>      open,
      
      ------------------ Receive Ports - FPGA RX Interface Ports -----------------
      gt0_rxusrclk_in                 =>      gt0_rxusrclk_in,
      gt0_rxusrclk2_in                =>      gt0_rxusrclk_in,
      ------------------ Receive Ports - FPGA RX interface Ports -----------------
      gt0_rxdata_out                  =>      gt0_rxdata_out,
      ------------------------ Receive Ports - RX AFE Ports ----------------------
      gt0_gthrxn_in                   =>      gt0_gthrxn_in,
      --------------------- Receive Ports - RX Equalizer Ports -------------------
      gt0_rxmonitorout_out            =>      open,
      gt0_rxmonitorsel_in             =>      "00", -------------
      gt0_rxoutclk_out                =>      gt0_rxoutclk_out,
      gt0_rxoutclkfabric_out          =>      open,--gt0_rxoutclkfabric_out,
      ------------- Receive Ports - RX Initialization and Reset Ports ------------
      gt0_gtrxreset_in                =>      gt0_gtrxreset_in,
      ------------------------ Receive Ports -RX AFE Ports -----------------------
      gt0_gthrxp_in                   =>      gt0_gthrxp_in,
      -------------- Receive Ports -RX Initialization and Reset Ports ------------
      gt0_rxresetdone_out             =>      gt0_rxresetdone_out,
      --------------------- TX Initialization and Reset Ports --------------------
      gt0_gttxreset_in                =>      gt0_gttxreset_in,
      gt0_txuserrdy_in                =>      gt0_txuserrdy_in,
      ------------------ Transmit Ports - FPGA TX Interface Ports ----------------
      gt0_txusrclk_in                 =>      gt0_txusrclk_in,
      gt0_txusrclk2_in                =>      gt0_txusrclk_in,
      ------------------ Transmit Ports - TX Data Path interface -----------------
      gt0_txdata_in                   =>      gt0_txdata_in,
      ---------------- Transmit Ports - TX Driver and OOB signaling --------------
      gt0_gthtxn_out                  =>      gt0_gthtxn_out,
      gt0_gthtxp_out                  =>      gt0_gthtxp_out,
      ----------- Transmit Ports - TX Fabric Clock Output Control Ports ----------
      gt0_txoutclk_out                =>      gt0_txoutclk_out,
      gt0_txoutclkfabric_out          =>     open,-- gt0_txoutclkfabric_out,
      gt0_txoutclkpcs_out             =>      open,--gt0_txoutclkpcs_out,
      ------------- Transmit Ports - TX Initialization and Reset Ports -----------
      gt0_txresetdone_out             =>      gt0_txresetdone_out,

      gt0_loopback_in                 =>      gt0_loopback_in,
      gt0_rxcdrhold_in                =>      gt0_rxcdrhold_in,
      gt0_qplloutclk_in               =>      '0',--gt0_qplloutclk_in,
      gt0_qplloutrefclk_in            =>      '0'--gt0_qplloutrefclk_in
      );


 
end RTL;    
 
