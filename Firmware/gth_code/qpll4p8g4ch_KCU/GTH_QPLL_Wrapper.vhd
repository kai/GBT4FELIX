--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2016/02/24 04:43:14 PM
-- Design Name: KCU QPLL GTH Wrapper  (Tx Low Latency, RX with inside buffer)
-- Module Name: GTH_QPLL_Wrapper_KCU - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The TOP MODULE: KCU QPLL GTH (for GBT)
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.FELIX_gbt_package.all;

use work.FELIX_gbt_package.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity GTH_QPLL_Wrapper is
  Port ( 
  
  
    --cpllreset_in                           : in std_logic_vector(0 downto 0);  
    --cpllfbclklost_out                      : out std_logic_vector(0 downto 0);
    --cplllock_out                           : out std_logic_vector(0 downto 0);
       
    gt_rxusrclk_in                         : in std_logic_vector(3 downto 0);
    gt_txusrclk_in                         : in std_logic_vector(3 downto 0);
    gt_rxoutclk_out                        : out std_logic_vector(3 downto 0);
    gt_txoutclk_out                        : out std_logic_vector(3 downto 0);
    gthrxn_in                              : in std_logic_vector(3 downto 0);
    gthrxp_in                              : in std_logic_vector(3 downto 0);
    gthtxn_out                             : out std_logic_vector(3 downto 0);
    gthtxp_out                             : out std_logic_vector(3 downto 0);
     
    drpclk_in                              : in std_logic_vector(0 downto 0);
    gtrefclk0_in                           : in std_logic_vector(0 downto 0);
     
    rxpolarity_in                          : in std_logic_vector(3 downto 0);
    txpolarity_in                          : in std_logic_vector(3 downto 0);  
    loopback_in                            : in std_logic_vector(2 downto 0);
    rxcdrhold_in                           : in std_logic;   
    userdata_tx_in                         : in std_logic_vector(79 downto 0);
    userdata_rx_out                        : out std_logic_vector(79 downto 0);
     
    userclk_rx_reset_in                    : in std_logic_vector(0 downto 0);
    userclk_tx_reset_in                    : in std_logic_vector(0 downto 0);
    
    -- reset_clk_freerun_in                 : in std_logic_vector(0 downto 0);
    reset_all_in                            : in std_logic_vector(0 downto 0);
    reset_tx_pll_and_datapath_in            : in std_logic_vector(0 downto 0);
    reset_tx_datapath_in                    : in std_logic_vector(0 downto 0);
    reset_rx_pll_and_datapath_in            : in std_logic_vector(0 downto 0);
    reset_rx_datapath_in                    : in std_logic_vector(0 downto 0);
    qpll1lock_out,qpll1fbclklost_out        : out std_logic_vector(0 downto 0);
    qpll0lock_out,qpll0fbclklost_out        : out std_logic_vector(0 downto 0);
  
    rxslide_in                              : in std_logic_vector(3 downto 0);
   
    txresetdone_out, txpmaresetdone_out     : out std_logic_vector(3 downto 0);
    rxresetdone_out ,rxpmaresetdone_out     : out std_logic_vector(3 downto 0);
     
    reset_tx_done_out                       : out std_logic_vector(0 downto 0);
    reset_rx_done_out                       : out std_logic_vector(0 downto 0);
    reset_rx_cdr_stable_out                 : out std_logic_vector(0 downto 0);
    rxcdrlock_out                           : out std_logic_vector(3 downto 0)
  
    );
end GTH_QPLL_Wrapper;

architecture Behavioral of GTH_QPLL_Wrapper is

  component KCU_RXBUF_PMA_QPLL_4CH is
    port(
      gtwiz_userclk_tx_active_in            : in std_logic_vector(0 downto 0);
      gtwiz_userclk_rx_active_in            : in std_logic_vector(0 downto 0);
      gtwiz_buffbypass_tx_reset_in          : in std_logic_vector(0 downto 0);
      gtwiz_buffbypass_tx_start_user_in     : in std_logic_vector(0 downto 0);
      gtwiz_buffbypass_tx_done_out          : out std_logic_vector(0 downto 0);
      gtwiz_buffbypass_tx_error_out         : out std_logic_vector(0 downto 0);
      gtwiz_reset_clk_freerun_in            : in std_logic_vector(0 downto 0);
      gtwiz_reset_all_in                    : in std_logic_vector(0 downto 0);
      gtwiz_reset_tx_pll_and_datapath_in    : in std_logic_vector(0 downto 0);
      gtwiz_reset_tx_datapath_in            : in std_logic_vector(0 downto 0);
      gtwiz_reset_rx_pll_and_datapath_in    : in std_logic_vector(0 downto 0);
      gtwiz_reset_rx_datapath_in            : in std_logic_vector(0 downto 0);
      gtwiz_reset_rx_cdr_stable_out         : out std_logic_vector(0 downto 0);
      gtwiz_reset_tx_done_out               : out std_logic_vector(0 downto 0);
      gtwiz_reset_rx_done_out               : out std_logic_vector(0 downto 0);
      gtwiz_userdata_tx_in                  : in std_logic_vector(79 downto 0);
      gtwiz_userdata_rx_out                 : out std_logic_vector(79 downto 0);
      gtrefclk01_in                         : in std_logic_vector(0 downto 0);
      qpll1outclk_out                       : out std_logic_vector(0 downto 0);
      qpll1outrefclk_out                    : out std_logic_vector(0 downto 0);
      loopback_in                           : in std_logic_vector(11 downto 0);
      gthrxn_in                             : in std_logic_vector(3 downto 0);
      gthrxp_in                             : in std_logic_vector(3 downto 0);
      rxcdrhold_in                          : in std_logic_vector(3 downto 0);
      rxpolarity_in                         : in std_logic_vector(3 downto 0);
      rxusrclk_in                           : in std_logic_vector(3 downto 0);
      rxusrclk2_in                          : in std_logic_vector(3 downto 0);
      txpolarity_in                         : in std_logic_vector(3 downto 0);
      txusrclk_in                           : in std_logic_vector(3 downto 0);
      qpll1lock_out                         : out std_logic_vector(0 downto 0);
      qpll1fbclklost_out                    : out std_logic_vector(0 downto 0);
      qpll0fbclklost_out                    : out std_logic_vector(0 downto 0);
      qpll0lock_out                         : out std_logic_vector(0 downto 0);
      txusrclk2_in                          : in std_logic_vector(3 downto 0);
      gthtxn_out                            : out std_logic_vector(3 downto 0);
      gthtxp_out                            : out std_logic_vector(3 downto 0);
      rxslide_in                            : in std_logic_vector(3 downto 0);
      rxcdrlock_out                         : out std_logic_vector(3 downto 0);
      rxoutclk_out                          : out std_logic_vector(3 downto 0);
      rxpmaresetdone_out                    : out std_logic_vector(3 downto 0);
      txoutclk_out                          : out std_logic_vector(3 downto 0);
      txresetdone_out                       : out std_logic_vector(3 downto 0);
      rxresetdone_out                       : out std_logic_vector(3 downto 0);
      txpmaresetdone_out                    : out std_logic_vector(3 downto 0)
      );
  end component;


  signal rxusrclk               : std_logic_vector(3 downto 0);
  signal rxusrclk_int           : std_logic_vector(3 downto 0);
  signal rxusrclk2_int          : std_logic_vector(3 downto 0);
  signal txoutclk_int           : std_logic_vector(3 downto 0);
  signal rxoutclk_int           : std_logic_vector(3 downto 0);
  signal txusrclk_int           : std_logic_vector(3 downto 0);
  signal txusrclk2_int          : std_logic_vector(3 downto 0);
  signal userclk_tx_active_out  : std_logic_vector(0 downto 0);
  signal userclk_rx_active_out  : std_logic_vector(0 downto 0);
  signal userclk_rx_active_out_p: std_logic_vector(0 downto 0);
  signal userclk_tx_active_out_p: std_logic_vector(0 downto 0);
  signal txusrclk               : std_logic;
  signal vccvec                 : std_logic_vector(0 downto 0);
  signal gndvec                 : std_logic_vector(0 downto 0);

begin

  vccvec(0)     <= '1';
  gndvec(0)     <= '0';
   -- RxUsrClk
  rxusrclk      <= gt_rxusrclk_in;
  rxusrclk_int  <= rxusrclk;
  rxusrclk2_int <= rxusrclk;
  
  gt_rxoutclk_out <= rxoutclk_int;
  gt_txoutclk_out <= txoutclk_int;
  
--  -- TxUsrClk
--  tx_usrclk_bufg: bufg_gt
--  port map(
--    i => txoutclk_int(0),
--    div =>"000",
--    clr =>'0',--userclk_tx_reset_in,--'0',
--    cemask =>'0',
--    clrmask=>'0',
--    ce=>'1',
--    o => txusrclk
--  );
  
  txusrclk      <= gt_txusrclk_in(0);
  txusrclk2_int <= gt_txusrclk_in;
  txusrclk_int  <= gt_txusrclk_in;

  process(userclk_tx_reset_in(0), txusrclk)
  begin
    if userclk_tx_reset_in(0) = '1' then
      userclk_tx_active_out(0)          <= '0';
    elsif txusrclk'event and txusrclk = '1' then
      userclk_tx_active_out_p(0)        <= '1';
      userclk_tx_active_out             <= userclk_tx_active_out_p;
    end if;
  end process;
  
  process(userclk_rx_reset_in(0), rxusrclk(0))
  begin
    if userclk_rx_reset_in(0) = '1' then
      userclk_rx_active_out(0)          <= '0';
    elsif rxusrclk(0)'event and rxusrclk(0) = '1' then
      userclk_rx_active_out_p(0)        <= '1';
      userclk_rx_active_out             <= userclk_rx_active_out_p;
    end if;
  end process;
 
 
 
   


--RXBUF_GEN: if KCU_LOWER_LATENCY = '0' generate 

  gtwizard_ultrascale_four_channel_qpll_inst:  KCU_RXBUF_PMA_QPLL_4CH
    port map(
      gtwiz_userclk_tx_active_in            => userclk_tx_active_out,
      gtwiz_userclk_rx_active_in            => userclk_rx_active_out,
      gtwiz_buffbypass_tx_reset_in          => gndvec,
      gtwiz_buffbypass_tx_start_user_in     => gndvec,
      gtwiz_buffbypass_tx_done_out          => open,
      gtwiz_buffbypass_tx_error_out         => open,
  
      gtwiz_reset_clk_freerun_in            => drpclk_in,
      gtwiz_reset_all_in                    => reset_all_in,
  
      gtwiz_reset_tx_pll_and_datapath_in    => reset_tx_pll_and_datapath_in,
      gtwiz_reset_tx_datapath_in            => reset_tx_datapath_in,
      gtwiz_reset_rx_pll_and_datapath_in    => reset_rx_pll_and_datapath_in,
      gtwiz_reset_rx_datapath_in            => reset_rx_datapath_in,
  
      gtwiz_reset_rx_cdr_stable_out         => reset_rx_cdr_stable_out,
      gtwiz_reset_tx_done_out               => reset_tx_done_out,
      gtwiz_reset_rx_done_out               => reset_rx_done_out,
  
      qpll1lock_out                         => qpll1lock_out,
      qpll0lock_out                         => qpll0lock_out,
      qpll1fbclklost_out                    => qpll1fbclklost_out,
      qpll0fbclklost_out                    => qpll0fbclklost_out,
      gtwiz_userdata_tx_in                  => userdata_tx_in,
      gtwiz_userdata_rx_out                 => userdata_rx_out,
      gtrefclk01_in                         => gtrefclk0_in,
      loopback_in                           => loopback_in & loopback_in & loopback_in & loopback_in,
      qpll1outclk_out                       => open,
      qpll1outrefclk_out                    => open,
      gthrxn_in                             => gthrxn_in,
      gthrxp_in                             => gthrxp_in,
      rxcdrhold_in                          => (others=>rxcdrhold_in),
      rxpolarity_in                         => rxpolarity_in,
      rxusrclk_in                           => rxusrclk_int,
      rxusrclk2_in                          => rxusrclk2_int,
      txpolarity_in                         => txpolarity_in,
      txusrclk_in                           => txusrclk_int,
      txusrclk2_in                          => txusrclk2_int,
      gthtxn_out                            => gthtxn_out,
      gthtxp_out                            => gthtxp_out,
      rxcdrlock_out                         => rxcdrlock_out,
      rxoutclk_out                          => rxoutclk_int,
      rxpmaresetdone_out                    => rxpmaresetdone_out,
      rxslide_in                            => rxslide_in,
      txoutclk_out                          => txoutclk_int,
      txpmaresetdone_out                    => txpmaresetdone_out,
      txresetdone_out                       => txresetdone_out,
      rxresetdone_out                       => rxresetdone_out
      );
--end generate;

end Behavioral;
