# README
This repository is about the use of optimized *GBT-FPGA* core for *FELIX* project in *ATLAS* experiment.  

**For single channel example on KC705, please check https://github.com/simpway/GBT_KC705**  
Latency: **Tx: around 27.8~32 ns** (latency of the time domain crossing from TX 40M to TX 240M is depends by the phase relationship between these two clocks, but it is fixed). **Rx around: 43.9 ns for WB mode, 56.4 ns (no timing error) for FEC mode.**   
More flexible GBT mode switching is supported. 


## Introduction
### Q&A
1. Where is this IP core used? 
	This IP is part of FELIX firmware.
   
2. Special conditions of FELIX.
 	- The configuration (if needed) of this GBT core is done via PCIe registers, by software. User should consider how to do it in his project.
 	- The FELIX is used as Back-End, it distributes the LHC clock to all of the Front-Ends. The reference clock for links from FE to FELIX is required to be synchronized with the LHC clock. 
 	  + FELIX can directly use a local 240M clock (synchronized with LHC clock) as RXUSRCLK for all channels; or use the recovered RXOUTCLK from master channel as RXUSRCLK for all channels.
 	
    
3. What is the difference between GBT part of this IP and the CERN GBT-FPGA IP core 3.0.2.
  - Based on 3.0.2. The Scrambler/Descrambler is almost from CERN version. The FEC encoder/decoder is from CERN version.
  - The GTX IP is removed, to make the GBT independent with the transceiver.
  - The dynamic change of the GBT mode between normal FEC mode and Wide-Bus mode is provided, without FPGA reprogramming.
  - A new RxGearBox with Rx alignment function are designed to replace the RxGearBox and framealigner provided by CERN GBT-FPGA. The alignment operation can be automatically done by firmware or controlled by software.
  - The scrambler and descrambler are changed from 40 MHz clock domain to 240 MHz domain. Enable signal are added to control the descrambler and scrambler.
  - The new time domain crossing between 40 MHz and 240 MHz are added in scrambler and descrambler.
  
4. For Virtex 7, about the GTH IP: 
	- The TxOutClk of the master channel or external 240 MHz clock can be used to drive the TxUsrClk.
	- The RxOutClk of the master channel or external 240 MHz clock can be used to drive the RxUsrClk.
	- The bitslip is needed for the GTH output data. When the bitslip is finished, a stable GBT frame header is locked. When external RxUsrClk is used, at this time the phase between RxOutClk and RXUSRCLK is uncertain. A multiplexer is added to the GBT RxGearBox, to solve the time domain crossing problem for the 20 bit data transferred from RxOutClk to RxUsrClk in the GTH hard core.
	- The GTH IP based on both of CPLL and QPLL are provided. Since MGTRefclk on HTG-710 can't be from TTC clock. GRefClk is the default configuration. Quad is the unit for the GTH IP.
	
5. For GTH in UltraScale:
  - Lower latency version: Tx and Rx buffer in GTH are disabled. Each channel use its own TXOUTCLK as TXUSRCLK, and RXOUTCLK as RXUSRCLK, since KCU has >1000 BUFGs.
  
6. About the training of the GBT core RX block.
  - The Virtex RX low-latency version, training can be done by software (python script, or tool based on C language), or firmware (automatically when Rx link is reset, but the GBT alignment and error stataus registers may need a manually reset, since the CDRLOCK is not OK for this version GTH IP from Xilinx).
  - See the slides in Docs for details.

7. Tx latency adjusting
  - TX\_TC\_DLY\_VALUE can be adjusted (phase 0~6), to get a stable and lowest Tx latency. When TX\_TC\_MODE is '0' (one time phase selection), TX\_RESET to GBT encoding should be done after that. When TX\_TC\_MODE is '1' (cycle by cycle phase selection), the TC\_EDGE should be selected, depend on the phase between the TX 40MHz clock and TXUSRCLK, one of '0' and '1' may be not good. User should test then choose the TC\EDGE, the phase between these two clocks are fixed, so the TC\_EDGE doesn't need to be changed.

8. Data transfer after the RXUSRCLK
  - For FELIX, all data will be transferred to a local LHC synchronized 40MHz clock, so the process of the domain crossing is given in the manual. 
  - For FE, if you want to transfer data from the 240M domain to a synchronized domain with different frequency, one simple way is to make all these 120/240/160/320... clocks be aligned with the recovered 40MHz (very easy in FPGA). For GBT, the packet has a GBT header, so the phase of recovered 40MHz is fixed to the back-end. This make all of the clocks and data to be synchronized, and latency fixed. For other similar protocol like 8B10B, similar things can be done.

  
### Architecture of this IP
- Firmware:
  + GBT core: encoding+decoding
  + GTH core: files for different FPGA.
- Software:
  + The C language tools are not given
  + The python examples are given. But they are based on the FELIX, PCIe registers are needed. This is a backup tool for the RX low-latency version, though the RX alignment can be done automatically.
   

## Status
- 20160917: TX side time domain crossing block is redesigned
- An extra latency adjust block will be added, since some Front-End ask for a delay adjustment with a step of 1/240MHz.
- The delay adjustment with step of 1/4.8GHz is also under testing.

