--!-----------------------------------------------------------------------------
--!                                                                           --
--!           BNL - Brookhaven National Lboratory                             --
--!                       Physics Department                                  --
--!                         Omega Group                                       --
--!-----------------------------------------------------------------------------
--|
--! author:      Kai Chen    (kchen@bnl.gov)
--!
--!
--!-----------------------------------------------------------------------------
--
--
-- Create Date: 2014/12/05 04:43:14 PM
-- Design Name: GBT Tx Time domain crossing Top
-- Module Name: gbt_tx_timedomaincrossing_FELIX - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions: Vivado
-- Description:
--              The TX TIME DOMAIN CROSSING MODULE for GBT
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- Copyright: All rights reserved
----------------------------------------------------------------------------------

library ieee;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;
 
use work.FELIX_gbt_package.all;

entity gbt_tx_timedomaincrossing_FELIX is
  generic
    (
      channel                   : integer := 0
      );
  port
    (
      Tx_Align_Signal           : out std_logic;
      TX_TC_METHOD              : in std_logic;
      TX_LATOPT_TC              : in  std_logic;
      
      TC_EDGE                   : in std_logic;
      
      TX_TC_DLY_VALUE           : in std_logic_vector(2 downto 0);  
      
      TX_WORDCLK_I              : in  std_logic;
      TX_RESET_I                : in  std_logic;
      TX_FRAMECLK_I             : in  std_logic
      
      --TX_ISDATA_SEL_I         : in  std_logic;
     
      );
end gbt_tx_timedomaincrossing_FELIX;


architecture Behavior of gbt_tx_timedomaincrossing_FELIX is   


  signal fsm_rst                : std_logic := '0';
  signal TX_FRAMECLK_I_4r       : std_logic;
  signal TX_FRAMECLK_I_5r       : std_logic;
  signal TX_FRAMECLK_I_2r       : std_logic;
  signal TX_FRAMECLK_I_r        : std_logic;
  signal cnt                    : std_logic_vector(2 downto 0) :="000";
  signal TX_TC_DLY_VALUE_i      : std_logic_vector(2 downto 0) :="000";
  signal TX_RESET_FLAG_clr      : std_logic;
  signal TX_RESET_FLAG          : std_logic;
  signal TX_RESET_r             : std_logic;
  signal TX_RESET_2r            : std_logic;
  signal pulse_rising           : std_logic;
  signal pulse_rising_r         : std_logic;
  signal pulse_falling_r        : std_logic;
  signal pulse_falling          : std_logic;
  
 
begin                

  -----------------------------------------------------------------------------
  -- Alignment signal for TX GearBox
    -----------------------------------------------------------------------------

  TX_TC_DLY_VALUE_i     <= TX_TC_DLY_VALUE when TX_DLY_SW_CTRL='1' else TX_TC_DLY_VALUE_package;

  process(TX_WORDCLK_I)
  begin    
    if TX_WORDCLK_I'event and TX_WORDCLK_I='0' then  
      TX_FRAMECLK_I_4r  <= TX_FRAMECLK_I;
      TX_FRAMECLK_I_5r  <= TX_FRAMECLK_I_4r;
    end if;
  end process;   
    
  process(TX_WORDCLK_I)
  begin    
    if TX_WORDCLK_I'event and TX_WORDCLK_I='1' then    
      TX_FRAMECLK_I_r   <= TX_FRAMECLK_I;
      TX_FRAMECLK_I_2r  <= TX_FRAMECLK_I_r;
      TX_RESET_r        <= TX_RESET_I;
      TX_RESET_2r       <= TX_RESET_r;
      if TX_RESET_r='1' and TX_RESET_2r='0' then
        TX_RESET_FLAG   <='1';
      elsif  TX_RESET_FLAG_clr='1' then
        TX_RESET_FLAG   <='0';
      end if;
      pulse_rising      <= TX_FRAMECLK_I_r and (not TX_FRAMECLK_I_2r);
      pulse_falling     <= TX_FRAMECLK_I_4r and (not TX_FRAMECLK_I_5r);
      pulse_rising_r    <= pulse_rising;
      pulse_falling_r   <= pulse_falling;
      if (TX_TC_METHOD='1' or TX_RESET_FLAG='1') then
        if TC_EDGE='0' then
          TX_RESET_FLAG_clr     <= pulse_rising; 
        else
          TX_RESET_FLAG_clr     <= pulse_falling_r;
        end if;
      else 
        TX_RESET_FLAG_clr       <= '0';          
      end if;
    end if;
  end process;         
            
  process(TX_WORDCLK_I)
  begin            
    if TX_WORDCLK_I'event and TX_WORDCLK_I='1' then 
      if TX_RESET_FLAG_clr='1' then
        cnt     <= TX_TC_DLY_VALUE_i;
        if TX_TC_DLY_VALUE_i = "011" then
          Tx_Align_Signal       <= '1';
        else
          Tx_Align_Signal       <= '0';
        end if;
      else        
        case cnt is
          when "000" =>
            cnt                 <= "001";
            fsm_rst             <= '0';
            Tx_Align_Signal     <= '0';
          -- tx_frameclk_i_shifted <='1';
          when "001" =>
            cnt                 <= "010";
            fsm_rst             <= '0';
            Tx_Align_Signal     <= '0';
          -- tx_frameclk_i_shifted <='1';
          when "010" =>
            cnt                 <= "011";
            fsm_rst             <= '0';
            Tx_Align_Signal     <= '1';
          -- tx_frameclk_i_shifted <='0';
          when "011" =>
            cnt                 <= "100";
            fsm_rst             <= '0';
            Tx_Align_Signal     <= '0';
          -- tx_frameclk_i_shifted <='0';
          when "100" =>
            cnt                 <= "101";
            fsm_rst             <= '0';
            Tx_Align_Signal     <= '0';
          -- tx_frameclk_i_shifted <='0';
          when "101" =>
            cnt <="000";
            fsm_rst             <= '0';
            Tx_Align_Signal     <= '0';
          -- tx_frameclk_i_shifted <= '1';
          when others =>
            cnt                 <= "101";
            fsm_rst             <= '1';
            Tx_Align_Signal     <= '0';
        end case;
      end if; 
    end if;
  end process;
    
 
   

   --=====================================================================================--
end Behavior;
