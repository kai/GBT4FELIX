###############################################################################
# User Configuration
# Link Width   - x8
# Link Speed   - gen3
# Family       - virtex7
# Part         - xc7vx690t
# Package      - ffg1761
# Speed grade  - -2
# PCIe Block   - X0Y1
###############################################################################
#
###############################################################################
# User Constraints
###############################################################################

# SFP disable pins active high
#set_property PACKAGE_PIN AB41 [get_ports SFP1_TX_DISABLE_H]
#set_property IOSTANDARD LVCMOS18 [get_ports SFP1_TX_DISABLE_H]
#set_property PACKAGE_PIN Y42 [get_ports SFP2_TX_DISABLE_H]
#set_property IOSTANDARD LVCMOS18 [get_ports SFP2_TX_DISABLE_H]
#set_property PACKAGE_PIN AC38 [get_ports SFP3_TX_DISABLE_H]
#set_property IOSTANDARD LVCMOS18 [get_ports SFP3_TX_DISABLE_H]
#set_property PACKAGE_PIN AC40 [get_ports SFP4_TX_DISABLE_H]
#set_property IOSTANDARD LVCMOS18 [get_ports SFP4_TX_DISABLE_H]

set_property PACKAGE_PIN AB41 [get_ports opto_inhibit[0]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_inhibit[0]]

set_property PACKAGE_PIN Y42 [get_ports opto_inhibit[1]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_inhibit[1]]

set_property PACKAGE_PIN AC38 [get_ports opto_inhibit[2]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_inhibit[2]]

set_property PACKAGE_PIN AC40 [get_ports opto_inhibit[3]]
set_property IOSTANDARD LVCMOS18 [get_ports opto_inhibit[3]]

#NET RX_CLK_TP LOC = AM31 | IOSTANDARD = "LVCMOS18";

#pull out CXP ports from the VC709 design
#set_property package_pin "" [get_ports [list  CXP2_RST_L]]
#set_property package_pin "" [get_ports [list  CXP1_RST_L]]

###############################################################################
# User Time Names / User Time Groups / Time Specs
###############################################################################

####################### GT reference clock constraints #######################

#Direct routing from ADN chip towards Si5324 jitter cleaner
set_property package_pin AT36 [get_ports si5324_resetn]
set_property iostandard lvcmos18 [get_ports si5324_resetn]
set_property PACKAGE_PIN AW32 [get_ports clk_adn_160_out_p]
set_property PACKAGE_PIN AW33 [get_ports clk_adn_160_out_n]
set_property IOSTANDARD LVDS [get_ports clk_adn_160_out_p]
set_property IOSTANDARD LVDS [get_ports clk_adn_160_out_n]
# VC709 clock from Si5324
# FELIX: this is the quad clocking around Q2 (plus above and below)
#set_property PACKAGE_PIN AH7 [get_ports Q2_CLK0_GTREFCLK_PAD_N_IN]
#set_property PACKAGE_PIN AH8 [get_ports Q2_CLK0_GTREFCLK_PAD_P_IN]
# VC709: clock from SMA (J25/J26)
# FELIX: this is the quad clocking around Q2 (plus above and below)
set_property PACKAGE_PIN AK7 [get_ports Q2_CLK0_GTREFCLK_PAD_N_IN]
set_property PACKAGE_PIN AK8 [get_ports Q2_CLK0_GTREFCLK_PAD_P_IN]
#TTCfx V3 from REF1 (FMC_HPC_LA00_P/N) [AB: can this work??]
#set_property PACKAGE_PIN K40 [get_ports Q2_CLK0_GTREFCLK_PAD_N_IN]
#set_property PACKAGE_PIN K39 [get_ports Q2_CLK0_GTREFCLK_PAD_P_IN]


###############################################################################
# GBT SFP Physical Constraints
###############################################################################
# SFP 1-4
set_property PACKAGE_PIN AN5 [get_ports {RX_N[1]}]
set_property PACKAGE_PIN AM7 [get_ports {RX_N[0]}]
set_property PACKAGE_PIN AL5 [get_ports {RX_N[2]}]
set_property PACKAGE_PIN AJ5 [get_ports {RX_N[3]}]


create_pblock pblock_5
add_cells_to_pblock [get_pblocks pblock_5] [get_cells -quiet [list {u22/gbtRxTx[0].gbtTxRx_inst} {g1.u2/gbtRxTx[1].gbtTxRx_inst}]]
add_cells_to_pblock [get_pblocks pblock_5] [get_cells -quiet [list {u22/gbtRxTx[2].gbtTxRx_inst} {g1.u2/gbtRxTx[3].gbtTxRx_inst}]]
resize_pblock [get_pblocks pblock_5] -add {SLICE_X170Y150:SLICE_X221Y199}


###############################################################################
# Physical Constraints
###############################################################################

#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" } ] 4
#set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" } ] 3

#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" } ] 4
#set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" } ] 3

#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" } ] 4
#set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" } ] 3

#set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" } ] 4
#set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" } ] 3

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/D" } ] 2

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*FelixDescrambler/RX_HEADER_O_reg[*]/D" } ] 2

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*gbtRxDescrambler21bit/feedbackRegister_reg[*]/D" } ] 2



set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixRxGearbox/reg_inv_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*error_buf_reg/D" } ] 2


set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[0].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 2
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[0].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 1
set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[1].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 2
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[1].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 1
set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[2].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 2
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler84bit_gen[2].gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 1

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[0].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[0].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 2
set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[1].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 3
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler32bit_gen[1].gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*TX_WORD_O_reg[*]/D" } ] 2

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" } ] 2
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler21bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" } ] 1

set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" } ] 2
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtTxScrambler16bit/feedbackRegister_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*tx_buffer_reg[*]/D" } ] 1



set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" } ] 1
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler21bit/RX_DATA_I_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" } ] 0
set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" } ] 1
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*gbtRxDescrambler16bit/RX_EXTRA_DATA_WIDEBUS_I_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" } ] 0
set_multicycle_path  -setup -start -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" } ] -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" } ] 1
set_multicycle_path  -hold  -end -from [get_pins -hierarchical -filter { NAME =~  "*FelixDescrambler/RX_HEADER_O_reg[*]/C" } ]   -to [get_pins -hierarchical -filter { NAME =~ "*s_rx_120b_out_f00_reg[*]/D" } ] 0



##Setup

#create_pblock pblock_1
#add_cells_to_pblock [get_pblocks pblock_1] [get_cells -quiet [list {g1.u2/GTH_inst[0].GTH_TOP_INST} {g1.u2/GTH_inst[1].GTH_TOP_INST} {g1.u2/gbtRxTx[0].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[1].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[2].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[3].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[4].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[5].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[6].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[7].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox}]]
#resize_pblock [get_pblocks pblock_1] -add {SLICE_X206Y0:SLICE_X221Y149}
#resize_pblock [get_pblocks pblock_1] -add {DSP48_X17Y0:DSP48_X17Y59}
#resize_pblock [get_pblocks pblock_1] -add {RAMB18_X13Y0:RAMB18_X14Y59}
#resize_pblock [get_pblocks pblock_1] -add {RAMB36_X13Y0:RAMB36_X14Y29}

#create_pblock pblock_2
#add_cells_to_pblock [get_pblocks pblock_2] [get_cells -quiet [list {g1.u2/GTH_inst[2].GTH_TOP_INST} {g1.u2/GTH_inst[3].GTH_TOP_INST} {g1.u2/gbtRxTx[10].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[11].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[12].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[13].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[14].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[15].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[8].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox} {g1.u2/gbtRxTx[9].gbtTxRx_inst/gbtTx_inst/FELIXTxGearbox}]]
#resize_pblock [get_pblocks pblock_2] -add {SLICE_X206Y350:SLICE_X221Y499}
#resize_pblock [get_pblocks pblock_2] -add {DSP48_X17Y140:DSP48_X17Y199}
#resize_pblock [get_pblocks pblock_2] -add {RAMB18_X13Y140:RAMB18_X14Y199}
#resize_pblock [get_pblocks pblock_2] -add {RAMB36_X13Y70:RAMB36_X14Y99}

###############################################################################
# Timing Constraints
###############################################################################

#create_clock -period 6.250 -name ts_clk_adn_160 [get_nets clk_adn_160]

create_clock -name GTHREFCLK_1 -period 4.167 [get_pins g1.u2/GTHREFCLK_1.ibufds_instq2_clk0/O]

create_clock -name g1.u2/GT_TX_WORD_CLK[0] -period 4.167 [get_pins g1.u2/clk_generate[0].GTTXOUTCLK_BUFG/O]

#create_clock -name g1.u2/GT_TX_WORD_CLK[0] -period 4.167 [get_pins g1.u2/clk_generate[0].GTTXOUTCLK_BUFG/O]
#create_clock -name g1.u2/GT_TX_WORD_CLK[4] -period 4.167 [get_pins g1.u2/clk_generate[1].GTTXOUTCLK_BUFG/O]
#create_clock -name g1.u2/GT_TX_WORD_CLK[8] -period 4.167 [get_pins g1.u2/clk_generate[2].GTTXOUTCLK_BUFG/O]
#create_clock -name g1.u2/GT_TX_WORD_CLK[12] -period 4.167 [get_pins g1.u2/clk_generate[3].GTTXOUTCLK_BUFG/O]
#create_clock -name g1.u2/GT_TX_WORD_CLK[16] -period 4.167 [get_pins g1.u2/clk_generate[4].GTTXOUTCLK_BUFG/O]
#create_clock -name g1.u2/GT_TX_WORD_CLK[20] -period 4.167 [get_pins g1.u2/clk_generate[5].GTTXOUTCLK_BUFG/O]

#create_clock -name g1.u2/GT_TX_WORD_CLK[0] -period 4.167 [get_pins g1.u2/usrclk_inst[0].gthusrclk_gen/txclkgen_sys.txoutclk_bufg0_i/O]
#create_clock -name g1.u2/GT_TX_WORD_CLK[4] -period 4.167 [get_pins g1.u2/usrclk_inst[1].gthusrclk_gen/txclkgen_sys.txoutclk_bufg0_i/O]
#create_clock -name g1.u2/GT_TX_WORD_CLK[8] -period 4.167 [get_pins g1.u2/usrclk_inst[2].gthusrclk_gen/txclkgen_sys.txoutclk_bufg0_i/O]
#create_clock -name g1.u2/GT_TX_WORD_CLK[12] -period 4.167 [get_pins g1.u2/usrclk_inst[3].gthusrclk_gen/txclkgen_sys.txoutclk_bufg0_i/O]
#create_clock -name g1.u2/GT_TX_WORD_CLK[16] -period 4.167 [get_pins g1.u2/usrclk_inst[4].gthusrclk_gen/txclkgen_sys.txoutclk_bufg0_i/O]
#create_clock -name g1.u2/GT_TX_WORD_CLK[20] -period 4.167 [get_pins g1.u2/usrclk_inst[5].gthusrclk_gen/txclkgen_sys.txoutclk_bufg0_i/O]

#create_clock -name g1.u2/GT_RX_WORD_CLK[0] -period 4.167 [get_pins g1.u2/usrclk_inst[0].gthusrclk_gen/clkgen_sys.rxoutclk_bufg1_i/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[4] -period 4.167 [get_pins g1.u2/usrclk_inst[1].gthusrclk_gen/clkgen_sys.rxoutclk_bufg1_i/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[8] -period 4.167 [get_pins g1.u2/usrclk_inst[2].gthusrclk_gen/clkgen_sys.rxoutclk_bufg1_i/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[12] -period 4.167 [get_pins g1.u2/usrclk_inst[3].gthusrclk_gen/clkgen_sys.rxoutclk_bufg1_i/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[16] -period 4.167 [get_pins g1.u2/usrclk_inst[4].gthusrclk_gen/clkgen_sys.rxoutclk_bufg1_i/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[20] -period 4.167 [get_pins g1.u2/usrclk_inst[5].gthusrclk_gen/clkgen_sys.rxoutclk_bufg1_i/O]

#create_clock -name g1.u2/GT_RX_WORD_CLK[0] -period 4.167 [get_pins g1.u2/GTRXOUTCLK_BUFG0/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[1] -period 4.167 [get_pins g1.u2/GTRXOUTCLK_BUFG1/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[2] -period 4.167 [get_pins g1.u2/GTRXOUTCLK_BUFG2/O]
#create_clock -name g1.u2/GT_RX_WORD_CLK[3] -period 4.167 [get_pins g1.u2/GTRXOUTCLK_BUFG3/O]



set_false_path -from [get_clocks clk40_clk_wiz_40_0] -to [get_clocks {g1.u2/GT_TX_WORD_CLK[0]}]
set_false_path -from [get_clocks clk_out40_clk_wiz_40] -to [get_clocks {g1.u2/GT_TX_WORD_CLK[0]}]


set_false_path -from [get_clocks g1.u2/GT_TX_WORD_CLK[0]] -to [get_clocks clk40_clk_wiz_40_0]


set_false_path -from [get_clocks {g1.u2/GT_TX_WORD_CLK[0]}] -to [get_clocks clk_out40_clk_wiz_40]


###############################################################################
# Others
###############################################################################

# force Vivado to ignore usage of GTGREFCLK
set_property SEVERITY {Warning} [get_drc_checks REQP-44]
set_property SEVERITY {Warning} [get_drc_checks REQP-46]
# force vivado to ignore unplaced pins
#set_property SEVERITY {Warning} [get_drc_checks IOSTDTYPE-1]

###############################################################################
# End
###############################################################################
